
#pragma once

#define UINT32COLOR_ARGB(a,r,g,b) ((uint32)((((a)&0xff)<<24)|(((b)&0xff)<<16)|(((g)&0xff)<<8)|((r)&0xff)))
#define UINT32COLOR_RGBA(r,g,b,a) UINT32COLOR_ARGB(a,r,g,b)
#define UINT32COLOR_XRGB(r,g,b)   UINT32COLOR_ARGB(0xff,r,g,b)

#define UINT32COLOR_ALPHA(a) (uint8)((a& 0xFF000000) >> 24)
#define UINT32COLOR_RED(r)  (uint8)((r& 0x0000000FF))
#define UINT32COLOR_GREEN(g) (uint8)((g& 0x0000FF00) >> 8)
#define UINT32COLOR_BLUE(b) (uint8)((b& 0x00FF0000) >> 16)
