
#pragma once

#include <array>
#include <SDL.h>
#include <base/types.h>
#include <graphics/graphics_defines.h>


template<uint32 GRID_WIDTH, uint32 GRID_HEIGHT, int GRID_TILE_SIZE> class TGrid
{
public:

	static const uint32 WIDTH = GRID_WIDTH;
	static const uint32 HEIGHT = GRID_HEIGHT;
	static const uint32 TILE_SIZE = GRID_TILE_SIZE;

	void Clear(uint32 value);
	void DrawPixel(int x, int y, uint32 value);
	void DrawCircle(int x0, int y0, int radius, uint32 value);
	void DrawLine(int x0, int y0, int x1, int y1, uint32 value);

	void Paint(SDL_Renderer* renderer);

private:	

	void PaintTile(int x, int y, uint8 r, uint8 g, uint8 b, SDL_Renderer* renderer);
	uint8 Darken(uint8 value, uint8 amount);

	std::array<uint32, WIDTH * HEIGHT> m_Data;
};


typedef TGrid<24, 32, 32> CGridCanvas;


void CGridCanvas::Clear(uint32 value)
{
	for (uint32& p : m_Data)
		p = value;
}

void CGridCanvas::DrawPixel(int x, int y, uint32 value)
{
	if (x < 0 || y < 0 || x >= WIDTH || y >= HEIGHT)
		return;

	m_Data[x + y * WIDTH] = value;
}

void CGridCanvas::DrawCircle(int x0, int y0, int radius, uint32 value)
{
	int x = radius - 1;
	int y = 0;
	int dx = 1;
	int dy = 1;
	int err = dx - (radius << 1);

	DrawPixel(x0, y0, value);

	while (x >= y)
	{
		DrawPixel(x0 + x, y0 + y, value);
		DrawPixel(x0 + y, y0 + x, value);
		DrawPixel(x0 - y, y0 + x, value);
		DrawPixel(x0 - x, y0 + y, value);
		DrawPixel(x0 - x, y0 - y, value);
		DrawPixel(x0 - y, y0 - x, value);
		DrawPixel(x0 + y, y0 - x, value);
		DrawPixel(x0 + x, y0 - y, value);

		if (err <= 0)
		{
			y++;
			err += dy;
			dy += 2;
		}

		if (err > 0)
		{
			x--;
			dx += 2;
			err += dx - (radius << 1);
		}
	}
}

void CGridCanvas::DrawLine(int x0, int y0, int x1, int y1, uint32 value)
{
	const int dx = abs(x1 - x0);
	const int sx = x0 < x1 ? 1 : -1;
	const int dy = abs(y1 - y0);
	const int sy = y0 < y1 ? 1 : -1;
	int err = (dx > dy ? dx : -dy) / 2;
	int e2;

	for (;;)
	{
		if (x0 >= 0 && x0 < WIDTH && y0 >= 0 && y0 < HEIGHT)
		{
			const int grid_index = x0 + y0 * WIDTH;
			m_Data[grid_index] = value;
		}


		if (x0 == x1 && y0 == y1)
			break;

		e2 = err;

		if (e2 > -dx)
		{
			err -= dy;
			x0 += sx;
		}
		if (e2 < dy)
		{
			err += dx;
			y0 += sy;
		}
	}
}

uint8 CGridCanvas::Darken(uint8 value, uint8 amount)
{
	if (amount > value)
		return 0;
	return value - amount;
}

void CGridCanvas::PaintTile(int x, int y, uint8 r, uint8 g, uint8 b, SDL_Renderer* renderer)
{
	SDL_SetRenderDrawColor(renderer, r, g, b, 255);

	SDL_Rect rect;
	rect.x = x * TILE_SIZE;
	rect.y = y * TILE_SIZE;
	rect.w = TILE_SIZE;
	rect.h = TILE_SIZE;
	SDL_RenderFillRect(renderer, &rect);


	rect.x += 1;
	rect.y += 1;
	rect.w -= 2;
	rect.h -= 2;

	SDL_SetRenderDrawColor(renderer, Darken(r, 8), Darken(g, 8), Darken(b, 8), 255);
	SDL_RenderDrawRect(renderer, &rect);
}

void CGridCanvas::Paint(SDL_Renderer* renderer)
{
	int index = 0;
	for (int y = 0; y < HEIGHT; ++y)
	{
		for (int x = 0; x < WIDTH; ++x, ++index)
		{
			uint8 r = UINT32COLOR_RED(m_Data[index]);
			uint8 g = UINT32COLOR_GREEN(m_Data[index]);
			uint8 b = UINT32COLOR_BLUE(m_Data[index]);

			PaintTile(x, y, r, g, b, renderer);
		}
	}
}
