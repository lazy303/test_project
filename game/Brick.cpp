#include "Brick.h"
#include <SDL.h>

Brick::Brick(Vector2d position, Vector2d size)
{
    this->size = size;
    this->position = position;
    rec.h = size.y;
    rec.w = size.x;
    rec.x = position.x;
    rec.y = position.y;

    boundingBox.position = position;
    boundingBox.size = size;
}

void Brick::Update()
{
    boundingBox.position = position;
}

void Brick::Draw(SDL_Renderer* renderer)
{
    SDL_SetRenderDrawColor(renderer, 255, 255, 0, 255);
    SDL_RenderFillRectF(renderer, &rec);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderDrawRectF(renderer, &rec);
}
