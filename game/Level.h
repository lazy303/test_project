#pragma once
#include <vector>
#include "Vector2d.h"

class Level
{
public:
    Level(std::vector<Vector2d>& level);

    std::vector<Vector2d> level;
};