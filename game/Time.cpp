#include "Time.h"
#include <SDL.h>

unsigned int Time::last_time = 0;
float Time::dt = 0;

Time::Time()
{
    last_time = SDL_GetTicks();
}

float Time::DeltaTime()
{
    dt = ((float)SDL_GetTicks() - last_time);
    last_time = SDL_GetTicks();

    return dt;
}
