#pragma once
#include <SDL.h>

class Input
{
public:
    Input();

    static Input* Instance();
    
    bool KeyDown(SDL_Scancode scanCode);

    void Update();

    static void DeleteInput();

private:
    static Input* instance;
    const Uint8* keyboardState;
};