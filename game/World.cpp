#include "World.h"
#include "Entity.h"
#include <algorithm>

World::World(SDL_Renderer* renderer)
{
    this->renderer = renderer;
}

void World::Update()
{
    for (size_t i = 0; i < entityList.size(); i++)
    {
        entityList.at(i)->Update();
    }
}

void World::Draw()
{
    for (Entity* entity : entityList)
        entity->Draw(renderer);
}

void World::SpawnEntity(Entity* entity)
{
    entity->world = this;
    entityList.push_back(entity);
}

void World::DeleteEntity(Entity* entity)
{
    entityList.erase(std::remove(entityList.begin(), entityList.end(), entity),
        entityList.end());
    
    delete entity;
}

void World::DeleteAllEntities()
{
    for (Entity* entity : entityList)
    {
        delete entity;
    }
    entityList.clear();
}

World::~World()
{
    for (Entity* entity : entityList)
    {
        delete entity;
    }
}