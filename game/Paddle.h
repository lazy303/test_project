#pragma once
#include "Entity.h"
#include <SDL.h>

class Paddle : public Entity
{
public:
    Paddle(class Vector2d position, class Vector2d size);
    ~Paddle();

    virtual void Update() override;
    virtual void Draw(SDL_Renderer* renderer) override;

private:
    class Input* input;

    SDL_FRect paddle;

    const float speed = 3.f;
};