#include "Entity.h"

Entity::Entity(Vector2d position, Vector2d size) : position(position), size(size)
{
    //boundingBox.position = position;
    //boundingBox.size = size;
}

Vector2d Entity::GetCenter() const
{
    return Vector2d(position.x - (size.x * 0.5f), position.y - (0.5f * size.y));
}
