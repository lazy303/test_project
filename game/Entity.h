#pragma once
#include "Vector2d.h"
#include "BoundingBox.h"

class Entity
{
public:
    Entity(Vector2d position = Vector2d(0, 0), Vector2d size = Vector2d(0, 0));
    virtual void Update() = 0;
    virtual void Draw(struct SDL_Renderer* renderer) = 0;

    virtual Vector2d GetCenter() const;

    const inline BoundingBox GetBoundingBox() const { return boundingBox; }

    class World* world;

    friend bool operator==(const Entity& e1, const Entity& e2);

    Vector2d position;
protected:
    Vector2d size;
    BoundingBox boundingBox;
};

inline bool operator==(const Entity& e1, const Entity& e2)
{
    // Hittade inge b�ttre s�tt att g�ra det h�r p� eftersom Entity �r en abstract klass
    return &e1 == &e2;
}
