#include "Ball.h"
#include "Paddle.h"
#include "World.h"
#include "Brick.h"
#include "Time.h"

std::vector<Ball*> Ball::allBalls;

Ball::Ball(Vector2d position, Vector2d size)
{
    this->position = position;
    this->size = size;

    ball.h = size.y;
    ball.w = size.x;
    ball.x = position.x;
    ball.y = position.y;

    boundingBox.position = position;
    boundingBox.size = size;

    allBalls.push_back(this);
}

void Ball::Update()
{
    Move();

    Entity* collidingEntity = world->CheckCollision<Entity>(*this);
    if (collidingEntity == nullptr)
        return;

    CollisionResponse(position - collidingEntity->position);

    Paddle* paddlePtr = dynamic_cast<Paddle*>(collidingEntity);
    if (paddlePtr != nullptr)
    {
        PaddleCollision();
        return;
    }

    Brick* brickPtr = dynamic_cast<Brick*>(collidingEntity);
    if (brickPtr != nullptr)
    {
        BrickCollision(brickPtr);
        return;
    }
}

void Ball::Draw(SDL_Renderer* renderer)
{
    SDL_SetRenderDrawColor(renderer, color, color, color, 255);

    DrawCircle(renderer, (int)position.x, (int)position.y, 16);
}

void Ball::DrawCircle(SDL_Renderer* renderer, int32_t centreX, int32_t centreY, int32_t radius)
{
    const int32_t diameter = (radius * 2);

    centreX += (int)size.x / 2;
    centreY += (int)size.y / 2;

    int32_t x = (radius - 1);
    int32_t y = 0;
    int32_t tx = 1;
    int32_t ty = 1;
    int32_t error = (tx - diameter);

    while (x >= y)
    {
        SDL_RenderDrawPoint(renderer, centreX + x, centreY - y);
        SDL_RenderDrawPoint(renderer, centreX + x, centreY + y);
        SDL_RenderDrawPoint(renderer, centreX - x, centreY - y);
        SDL_RenderDrawPoint(renderer, centreX - x, centreY + y);
        SDL_RenderDrawPoint(renderer, centreX + y, centreY - x);
        SDL_RenderDrawPoint(renderer, centreX + y, centreY + x);
        SDL_RenderDrawPoint(renderer, centreX - y, centreY - x);
        SDL_RenderDrawPoint(renderer, centreX - y, centreY + x);

        if (error <= 0)
        {
            ++y;
            error += ty;
            ty += 2;
        }

        if (error > 0)
        {
            --x;
            tx += 2;
            error += (tx - diameter);
        }
    }
}

void Ball::PaddleCollision()
{
    color = 255;
}

void Ball::BrickCollision(Brick* collidingBrick)
{
    world->DeleteEntity(collidingBrick);
}

void Ball::CollisionResponse(Vector2d normal)
{
    float dot = Vector2d::Dot(direction, normal);
    if (dot > 0) return;

    Vector2d d = -normal * dot * 2.f;

    direction = direction.Normalize() + d;
}

void Ball::Move()
{
    position += direction.Normalize() * speed * Time::DeltaTime();
    boundingBox.position = position;
}
