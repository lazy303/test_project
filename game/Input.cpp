#include "Input.h"

Input* Input::instance = nullptr;

Input::Input()
{

}

Input* Input::Instance()
{
    if (instance == nullptr)
        instance = new Input();

    return instance;
}

bool Input::KeyDown(SDL_Scancode scanCode)
{
    return keyboardState[scanCode];
}

void Input::Update()
{
    keyboardState = SDL_GetKeyboardState(NULL);
}

void Input::DeleteInput()
{
    delete instance;
}
