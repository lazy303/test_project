#pragma once
#include "Entity.h"
#include <SDL.h>

class Vector2d;

class Brick : public Entity
{
public:
    Brick(Vector2d position, Vector2d size);

    virtual void Update() override;
    virtual void Draw(SDL_Renderer* renderer) override;

private:
    SDL_FRect rec;
};
