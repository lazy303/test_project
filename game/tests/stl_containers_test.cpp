
#include "stl_containers_test.h"
#include <base/Log.h>

#include <array>
#include <list>
#include <vector>
#include <map>
#include <string>

void NTest::StlContainers()
{
	NLog::Print("------------------------------------------\n");
	NLog::Print("STL CONTAINER TESTS\n");
	NLog::Print("------------------------------------------\n");

	{
		std::string a_string = "Hello";
		a_string.append(" world!");
		a_string += " Avalanche!";

		NLog::Print("string size:  %llu\n", a_string.size());
		NLog::Print("%s\n", a_string.c_str());


		for (char value : a_string)
		{
			NLog::Print("%c\n", value);
		}

		for (std::string::iterator it = a_string.begin(); it != a_string.end(); ++it)
		{
			NLog::Print("%c\n", *it);
		}

		NLog::Print("------------------------------------------\n");
	}

	{
		std::vector<int> a_vector_of_ints;
		a_vector_of_ints.reserve(128); //pre allocate space for 128 ints
		a_vector_of_ints.push_back(1);
		a_vector_of_ints.push_back(2);
		a_vector_of_ints.push_back(3);
		a_vector_of_ints.pop_back();

		NLog::Print("vector size:  %llu\n", a_vector_of_ints.size());
		NLog::Print("vector capacity:  %llu\n", a_vector_of_ints.capacity());

		for (int value : a_vector_of_ints)
		{
			NLog::Print("%i\n", value);
		}

		for (std::vector<int>::iterator it = a_vector_of_ints.begin(); it != a_vector_of_ints.end(); ++it)
		{
			NLog::Print("%i\n", *it);
		}

		NLog::Print("Value 0 : %i", a_vector_of_ints[0]);

		NLog::Print("------------------------------------------\n");
	}

	{
		std::array<int, 8> a_fixed_size_array_of_ints;
		a_fixed_size_array_of_ints[0] = 1;
		a_fixed_size_array_of_ints[1] = 2;
		a_fixed_size_array_of_ints[2] = 3;

		NLog::Print("array size:  %llu\n", a_fixed_size_array_of_ints.size());
		for (int value : a_fixed_size_array_of_ints)
		{
			NLog::Print("%i\n", value);
		}

		for (std::array<int, 8>::iterator it = a_fixed_size_array_of_ints.begin(); it != a_fixed_size_array_of_ints.end(); ++it)
		{
			NLog::Print("%i\n", *it);
		}

		NLog::Print("Value 0 : %i", a_fixed_size_array_of_ints[0]);

		NLog::Print("------------------------------------------\n");
	}

	{
		std::list<int> a_linked_list;
		a_linked_list.push_back(1);
		a_linked_list.push_back(2);
		a_linked_list.push_front(3);

		NLog::Print("list size:  %llu\n", a_linked_list.size());
		for (int value : a_linked_list)
		{
			NLog::Print("%i\n", value);
		}

		for (std::list<int>::iterator it = a_linked_list.begin(); it != a_linked_list.end(); ++it)
		{
			NLog::Print("%i\n", *it);
		}

		NLog::Print("------------------------------------------\n");
	}

	{
		std::map<std::string, int> a_dictionary;

		a_dictionary["number_1"] = 1;
		a_dictionary["number_2"] = 2;
		a_dictionary["number_3"] = 3;

		NLog::Print("map size:  %llu\n", a_dictionary.size());
		for (auto pair : a_dictionary)
		{
			NLog::Print("%s = %i\n", pair.first.c_str(), pair.second);
		}

		if (a_dictionary["number_1"] == 1)
		{
			NLog::Print("Yes!");
		}

		NLog::Print("------------------------------------------\n");
	}
}
