
#include "stl_weak_and_shared_pointers.h"
#include <base/Log.h>

#include <memory>
#include <string>


class CPointerTest
{
public:

	CPointerTest(const char* name)
		: m_Name(name)
	{
		NLog::Print("%s CONSTRUCTOR!\n", m_Name.c_str());
	}

	~CPointerTest()
	{
		NLog::Print("%s DESTRUCTOR!\n", m_Name.c_str());
	}

	const std::string& Name() const { return m_Name; }

private:

	std::string m_Name;
};

void NTest::StlSharedAndWeakPointers()
{
	NLog::Print("------------------------------------------\n");
	NLog::Print("STL SHARED & WEAK POINTER TESTS\n");
	NLog::Print("------------------------------------------\n");


	/*
		Shared pointers _own_ the data it points to. As long as the use_count of a shared pointer is > 0, the data lives.
		As soon as the use_count of a shared_pointer reaches 0, the data is deleted. (In this example we can see when the text DESTRUCTOR! is called)

		Weak pointers is a safe way of keeping a pointer to data without actually _owning_ it. To access the data a weak_pointer references, one needs to lock it (lock returns a shared_pointer)		
		As soon as the use_count of the shared_pointer that the weak_pointer references, reaches 0, the weak pointer lock() returns null.

		Using shared and weak pointer one can keep track of if an object is still alive

		BUT! Using shared pointers adds some performance penalties compared to regular pointers! And it may also make code more difficult to understand. 
		(Common problem is things not getting deleted when expected because some other system is keeping a shared reference to the data)
	*/

	//READ MORE:
	//https://docs.microsoft.com/en-us/cpp/cpp/how-to-create-and-use-shared-ptr-instances?view=vs-2019
	//https://docs.microsoft.com/en-us/cpp/cpp/how-to-create-and-use-weak-ptr-instances?view=vs-2019


	std::shared_ptr<CPointerTest> a_shared_ptr;
	std::weak_ptr<CPointerTest> a_weak_ptr;

	{
		a_shared_ptr.reset(new CPointerTest("A"));
		std::shared_ptr<CPointerTest> another_shared_ptr(new CPointerTest("B"));
		a_weak_ptr = a_shared_ptr;

		NLog::Print("%s shared_count: %lli\n", a_shared_ptr->Name().c_str(), a_shared_ptr.use_count());

		//Lock a_weak_ptr to access the data it references
		if (auto tmp_shared_ptr = a_weak_ptr.lock())
		{
			NLog::Print("%s shared_count: %lli\n", tmp_shared_ptr->Name().c_str(), tmp_shared_ptr.use_count());
			NLog::Print("%s weak_count: %lli\n", tmp_shared_ptr->Name().c_str(), a_weak_ptr.use_count());
		}

		another_shared_ptr = a_shared_ptr;

		NLog::Print("%s shared_count: %lli\n", a_shared_ptr->Name().c_str(), a_shared_ptr.use_count());
	}

	a_shared_ptr.reset();

	if (!a_shared_ptr)
	{
		NLog::Print("a_shared_ptr has been freed!\n");
	}

	//Check if the data a_weak_ptr references has been deleted
	if (a_weak_ptr.expired())
	{
		NLog::Print("a_weak_ptr is no longer referencing anything!\n");
	}
}

void NTest::StlUniquePointers()
{
	//READ MORE:
	//https://docs.microsoft.com/en-us/cpp/cpp/how-to-create-and-use-unique-ptr-instances?view=vs-2019

	NLog::Print("------------------------------------------\n");
	NLog::Print("STL UNIQUE POINTER TESTS\n");
	NLog::Print("------------------------------------------\n");

	std::unique_ptr<CPointerTest> a_unique_ptr;
	a_unique_ptr.reset(new CPointerTest("A"));

	std::unique_ptr<CPointerTest> another_unique_ptr;

	//This does not compile! We cannot copy a unique pointer! (Since it would no longer be unique)
	//another_unique_ptr = a_unique_ptr;

	another_unique_ptr = std::move(a_unique_ptr); // This moves the ownership of data from a_unique_ptr to another_unique_ptr

	if (!a_unique_ptr)
	{
		NLog::Print("a_unique_ptr is no longer referencing anything!\n");
	}

	if (another_unique_ptr)
	{
		NLog::Print("another_unique_ptr is referencing %s!\n", another_unique_ptr.get()->Name().c_str());
	}
}