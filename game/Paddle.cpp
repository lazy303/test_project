#include "Paddle.h"
#include "Input.h"
#include "Time.h"

Paddle::Paddle(Vector2d position, Vector2d size)
{
    input = Input::Instance();
    this->position = position;
    this->size = size;

    paddle.h = size.y;
    paddle.w = size.x;
    paddle.x = position.x;
    paddle.y = position.y;

    boundingBox.position = position;
    boundingBox.size = size;
}

void Paddle::Update()
{
    input->Update();

    if (input->KeyDown(SDL_SCANCODE_RIGHT))
        position.x += speed * Time::DeltaTime();
    else if (input->KeyDown(SDL_SCANCODE_LEFT))
        position.x -= speed * Time::DeltaTime();

    paddle.x = position.x;
    paddle.y = position.y;
    boundingBox.position = position;
}

void Paddle::Draw(SDL_Renderer* renderer)
{
    SDL_SetRenderDrawColor(renderer, 255, 255, 0, 255);
    SDL_RenderFillRectF(renderer, &paddle);
}
