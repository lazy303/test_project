#pragma once

class Time
{
public:
    Time();

    static float DeltaTime();

private:
    static unsigned int last_time;

    static float dt;
};