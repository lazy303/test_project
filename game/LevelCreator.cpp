#include "LevelCreator.h"
#include "World.h"
#include "Brick.h"
#include "BoundingBox.h"
#include "Paddle.h"
#include "Ball.h"

void LevelCreator::Update()
{
    if (SDL_PollEvent(evnt))
    {
        if (evnt->button.button == SDL_BUTTON_LEFT)
            SpawnBrick();
        else if (evnt->button.button == SDL_BUTTON_RIGHT)
            DestroyBrick();
        else if (evnt->key.keysym.scancode == SDL_SCANCODE_RETURN)
            SaveLevel();
    }
}

void LevelCreator::SpawnBrick()
{
    int mouseX = 0;
    int mouseY = 0;
    SDL_GetMouseState(&mouseX, &mouseY);

    while (mouseX % 32 != 0)
        mouseX--;
    while (mouseY % 32 != 0)
        mouseY--;

    for (Entity* entity : world->entityList)
    {
        BoundingBox box = entity->GetBoundingBox();
        if (box.ContainsPosition(Vector2d((float)mouseX, (float)mouseY)) ||
            box.ContainsPosition(Vector2d((float)mouseX + 64, (float)mouseY)))
            return;
    }
    world->SpawnEntity(new Brick(Vector2d((float)mouseX, (float)mouseY), Vector2d(96, 32)));
}

void LevelCreator::DestroyBrick()
{
    int mouseX = 0;
    int mouseY = 0;
    SDL_GetMouseState(&mouseX, &mouseY);
    for (Entity* entity : world->entityList)
    {
        if (entity->GetBoundingBox().ContainsPosition(Vector2d((float)mouseX, (float)mouseY)))
        {
            entity->world->DeleteEntity(entity);
            return;
        }
    }
}

void LevelCreator::StartGame()
{
    if (currentLevel >= levels.size())
        return;

    SaveLevel();

    for (Vector2d pos : levels.at(currentLevel)->level)
    {
        world->SpawnEntity(new Brick(Vector2d(pos), Vector2d(96, 32)));
    }

    world->SpawnEntity(new Paddle(Vector2d(304, 864), Vector2d(160, 32)));
    world->SpawnEntity(new Ball(Vector2d(384, 432), Vector2d(32, 32)));

    currentLevel++;
}

void LevelCreator::SaveLevel()
{
    if (world->entityList.empty())
        return;

    std::vector<Vector2d> finishedLevel;

    for (Entity* entity : world->entityList)
    {
        finishedLevel.push_back(Vector2d(entity->position.x, entity->position.y));
    }

    levels.push_back(new Level(finishedLevel));
    world->DeleteAllEntities();
}

LevelCreator::~LevelCreator()
{
    for (Level* level : levels)
    {
        delete level;
    }
}
