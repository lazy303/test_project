#include "GameManager.h"
#include "LevelCreator.h"
#include "World.h"
#include "Input.h"

GameManager::GameManager(World* world, LevelCreator* levelCreator)
{
    this->world = world;
    this->levelCreator = levelCreator;
    input = Input::Instance();
}

void GameManager::Update()
{
    input->Update();
    if (input->KeyDown(SDL_SCANCODE_SPACE) && state != GameState::Playing)
    {
        levelCreator->StartGame();
        state = GameState::Playing;
    }
    
    switch (state)
    {
    case CreatingLevel:
        levelCreator->Update();
        break;
    case Playing:
        world->Update();
        break;
    }
    world->Draw();
}
