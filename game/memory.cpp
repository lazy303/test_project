
#include <Base/Memory/MemoryManager.h>

#if 1

#include <Base/Memory/OSAPIForWindows.h>
#include <Base/Memory/OSAPIFastForWindows.h>
#include <Base/Memory/BitHelpers.h>

#pragma warning(disable : 4074) //warning C4074: initializers put in compiler reserved initialization area

//Make sure the memory manager is created before any other static objects
#pragma init_seg(compiler)
CMemoryManager<32, 32, 128, 256, 4096, COSAPIFastForWindows> g_MemoryManager(true);

#pragma warning(default:4074) 

void* operator new(size_t size)
{
	return static_cast<uint8*>(g_MemoryManager.Alloc((uint32)size));
}
//______________________________________________________________________________________

void* operator new[](size_t size)
{
	return static_cast<uint8*>(g_MemoryManager.Alloc((uint32)size));
}
//______________________________________________________________________________________

void operator delete(void* ptr)
{
	g_MemoryManager.Free(ptr);
}
//______________________________________________________________________________________

void operator delete[](void* ptr)
{
	g_MemoryManager.Free(ptr);
}
//______________________________________________________________________________________

void GetMemoryManagerAllocationInfo(SMemoryData& allocations, SMemoryData& allocated, SMemoryData& top_allocated)
{
	g_MemoryManager.GetAllocationInfo(allocations, allocated, top_allocated);
}
//______________________________________________________________________________________
#else
void GetMemoryManagerAllocationInfo(SMemoryData& allocations, SMemoryData& allocated, SMemoryData& top_allocated)
{
}
//______________________________________________________________________________________
#endif