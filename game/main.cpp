

#include <windows.h>
#include <SDL.h>

#include <base/types.h>
#include <graphics/grid_canvas/grid_canvas.h>

#include <vector>
#include <list>
#include <map>
#include <memory> //weak_ptr shared_ptr

#include "World.h"
#include "Brick.h"
#include "Paddle.h"
#include "Time.h"
#include "Ball.h"
#include "LevelCreator.h"
#include "Input.h"
#include "GameManager.h"
#include "tests/stl_containers_test.h"
#include "tests/stl_weak_and_shared_pointers.h"



int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, LPWSTR, int)
{	
	NTest::StlContainers();
	NTest::StlSharedAndWeakPointers();
	NTest::StlUniquePointers();

	CGridCanvas canvas;

	canvas.Clear(UINT32COLOR_XRGB(128, 128, 128));
	canvas.DrawLine(0, 0, CGridCanvas::WIDTH - 1, 0, UINT32COLOR_XRGB(32, 32, 32));
	canvas.DrawLine(CGridCanvas::WIDTH - 1, 0, CGridCanvas::WIDTH - 1, CGridCanvas::HEIGHT - 1, UINT32COLOR_XRGB(32, 32, 32));
	canvas.DrawLine(0, 0, 0, CGridCanvas::HEIGHT - 1, UINT32COLOR_XRGB(32, 32, 32));
	canvas.DrawLine(0, CGridCanvas::HEIGHT - 1, CGridCanvas::WIDTH - 1, CGridCanvas::HEIGHT - 1, UINT32COLOR_XRGB(32, 32, 32));

	
   
	const uint32 window_width = CGridCanvas::WIDTH * CGridCanvas::TILE_SIZE;
	const uint32 window_height = CGridCanvas::HEIGHT * CGridCanvas::TILE_SIZE;

	SDL_Event event;
	SDL_Renderer *renderer;
	SDL_Window *window;

	SDL_Init(SDL_INIT_VIDEO);
	SDL_CreateWindowAndRenderer(window_width, window_height, 0, &window, &renderer);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
	SDL_RenderClear(renderer);
		
	SDL_SetWindowTitle(window, "My game!");
	
    World world(renderer);
    LevelCreator levelCreator(&world, &event);
    GameManager manager(&world, &levelCreator);

	float paddle_x = 256.f;

	uint32 last_time = SDL_GetTicks();

	while (1)
    {
		const uint32 current_time = SDL_GetTicks();
		const float dt = (current_time - last_time) / 1000.f; //milliseconds to seconds
		
        Time::DeltaTime();

		canvas.Paint(renderer);
        manager.Update();

		if (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				break;
		}

		SDL_RenderPresent(renderer);
	}

    Input::DeleteInput();

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
