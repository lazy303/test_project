#pragma once
#include "Entity.h"
#include <vector>
#include <SDL.h>

class Ball : public Entity
{
public:
    Ball(class Vector2d position, class Vector2d size);

    virtual void Update() override;
    virtual void Draw(SDL_Renderer* renderer) override;

private:
    static std::vector<Ball*> allBalls;

    SDL_FRect ball;
    Vector2d direction = Vector2d(1, 0);
    float speed = 1;

    void DrawCircle(SDL_Renderer* renderer, int32_t centreX, int32_t centreY, int32_t radius);
    
    Uint8 color = 64;

    void PaddleCollision();
    void BrickCollision(class Brick* collidingBrick);

    void CollisionResponse(Vector2d normal);

    void Move();
};