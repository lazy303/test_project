#pragma once

enum GameState
{
    CreatingLevel,
    Playing
};

class GameManager
{
public:
    GameManager(class World* world, class LevelCreator* levelCreator);

    void Update();
private:
    class World* world;
    class LevelCreator* levelCreator;

    GameState state = GameState::CreatingLevel;

    class Input* input;
};