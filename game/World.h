#pragma once
#include <array>
#include <vector>
#include "BoundingBox.h"

class Entity;
struct SDL_Renderer;

class World
{
public:
    World(SDL_Renderer* renderer);
    ~World();

    void Update();
    void Draw();

    void SpawnEntity(Entity* entity);
    void DeleteEntity(Entity* entity);
    void DeleteAllEntities();

    template<typename T>
    T* CheckCollision(const Entity& otherEntity)
    {
        for (Entity* entity : entityList)
        {
            if (*entity == otherEntity)
                continue;

            if (otherEntity.GetBoundingBox().Collides(entity->GetBoundingBox()))
                return static_cast<T*>(entity);
        }
        return nullptr;
    }

    std::vector<Entity*> entityList;
private:

    SDL_Renderer* renderer;
};
