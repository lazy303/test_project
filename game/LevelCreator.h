#pragma once
#include <vector>
#include <list>
#include "Level.h"
#include <SDL.h>
#include <map>

class Vector2d;
class World;

class LevelCreator
{
public:
    inline LevelCreator(World* world, SDL_Event* evnt) { this->world = world; this->evnt = evnt; };
    ~LevelCreator();

    void Update();
    void StartGame();
private:
    World* world;
    SDL_Event* evnt;

    void SpawnBrick();
    void DestroyBrick();

    void SaveLevel();
    
    std::vector<Level*> levels;
    int currentLevel = 0;
    
};