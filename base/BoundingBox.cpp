#include "BoundingBox.h"
#include <math.h>

BoundingBox::BoundingBox() : position(Vector2d(0, 0)), size(Vector2d(0, 0))
{
}

BoundingBox::BoundingBox(Vector2d position, Vector2d size) : position(position), size(size)
{
}

bool BoundingBox::Collides(const BoundingBox other) const
{
    return other.position.x < position.x + size.x && other.position.x + other.size.x > position.x &&
        other.position.y < position.y + size.y && other.position.y + other.size.y > position.y;
}

bool BoundingBox::ContainsPosition(const Vector2d point) const
{
    return point.x >= position.x && point.x < position.x + size.x &&
        point.y >= position.y && point.y < position.y + size.y;
}
