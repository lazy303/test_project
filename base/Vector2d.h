#pragma once

class Vector2d
{
public:
    Vector2d();
    Vector2d(const Vector2d& v);
    Vector2d(float x, float y);

    float x, y;

    Vector2d operator+(const Vector2d& v) const;
    Vector2d operator-(const Vector2d& v) const;
    Vector2d operator*(const Vector2d& v) const;
    Vector2d operator/(const Vector2d& v) const;
    Vector2d operator=(const Vector2d& v);

    Vector2d operator*(float v) const;
    Vector2d& operator+=(const Vector2d& v);
    Vector2d operator-();

    float Length() const;
    Vector2d& Normalize();
    static float Dot(Vector2d& v1, Vector2d& v2);
};