#include "Vector2d.h"
#include <math.h>

Vector2d::Vector2d() : x(0), y(0)
{
}

Vector2d::Vector2d(const Vector2d& v) : x(v.x), y(v.y)
{
}

Vector2d::Vector2d(float x, float y) : x(x), y(y)
{
}

Vector2d Vector2d::operator+(const Vector2d& v) const
{
    return Vector2d(x + v.x, y + v.y);
}

Vector2d Vector2d::operator-(const Vector2d& v) const
{
    return Vector2d(x - v.x, y - v.y);
}

Vector2d Vector2d::operator*(const Vector2d& v) const
{
    return Vector2d(x * v.x, y * v.y);
}

Vector2d Vector2d::operator/(const Vector2d& v) const
{
    return Vector2d(x / v.x, y / v.y);
}

Vector2d Vector2d::operator=(const Vector2d& v)
{
    x = v.x;
    y = v.y;
    return *this;
}

Vector2d Vector2d::operator*(float v) const
{
    return Vector2d(x * v, y * v);
}

Vector2d& Vector2d::operator+=(const Vector2d& v)
{
    x += v.x;
    y += v.y;
    return *this;
}

Vector2d Vector2d::operator-()
{
    x = -x;
    y = -y;
    return Vector2d(x, y);
}

float Vector2d::Length() const
{
    float lengthSqrd = (x * x + y * y);
    return sqrtf(lengthSqrd);
}

Vector2d& Vector2d::Normalize()
{
    float length = Length();

    x /= length;
    y /= length;

    return *this;
}

float Vector2d::Dot(Vector2d& v1, Vector2d& v2)
{
    return v1.x * v2.x + v1.y * v2.y;
}
