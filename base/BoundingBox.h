#pragma once
#include "Vector2d.h"

class BoundingBox
{
public:
    BoundingBox();
    BoundingBox(Vector2d position, Vector2d size);

    Vector2d position;
    Vector2d size;

    bool Collides(const BoundingBox other) const;
    bool ContainsPosition(const Vector2d point) const;
};
