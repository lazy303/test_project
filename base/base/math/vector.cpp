
#include <math.h>
#include <base/math/vector.h>

static float InvSqrt(float value)
{
	return 1.0f / sqrtf(value);
}

SVector3f& SVector3f::Normalize()
{
	const float inv_sqrt = InvSqrt(x * x + y * y + z * z);
	x *= inv_sqrt;
	y *= inv_sqrt;
	z *= inv_sqrt;
	return *this;
}
//______________________________________________________________________________

float SVector3f::SquareDistanceXZ(const SVector3f& rhs) const
{
	return (x - rhs.x) * (x - rhs.x) + (z - rhs.z) * (z - rhs.z);
}
//______________________________________________________________________________
