
#pragma once

#include "math.h"


struct SVector2i
{
	int x;
	int y;
};

struct SVector3f
{
	SVector3f();
	SVector3f(const SVector3f& v);
	SVector3f(float x, float y, float z);
	
	operator float*();
	operator const float*() const;

	float& operator[](int i);
	const float& operator[](int i) const;

	const SVector3f operator-() const;
	const SVector3f operator*(float f) const;
	const SVector3f operator/(float f) const;
	const SVector3f operator+(const SVector3f& v) const;
	const SVector3f operator-(const SVector3f& v) const;
	const SVector3f operator*(const SVector3f& v) const;

	friend const SVector3f operator*(float f, const SVector3f& v);

	SVector3f& operator*=(float f);
	SVector3f& operator/=(float f);
	SVector3f& operator+=(const SVector3f& v);
	SVector3f& operator-=(const SVector3f& v);

	float LengthSquared() const;
	SVector3f& Normalize();
	float SquareDistanceXZ(const SVector3f& other) const;

	union
	{
		struct
		{
			float x, y, z;
		};
		float e[3];
	};
};

inline SVector3f::SVector3f() { }
inline SVector3f::SVector3f(float x, float y, float z) : x(x), y(y), z(z) { }
inline SVector3f::SVector3f(const SVector3f& v) : x(v.x), y(v.y), z(v.z) { }

inline SVector3f::operator float*()
{
	return (float*)&x;
}

inline SVector3f::operator const float*() const
{
	return (const float*)&x;
}

inline float& SVector3f::operator[](int i)
{
	return ((float*)&x)[i];
}

inline const float& SVector3f::operator[](int i) const
{
	return ((const float*)&x)[i];
}

inline const SVector3f SVector3f::operator-() const
{
	return SVector3f(-x, -y, -z);
}

inline const SVector3f SVector3f::operator*(float f) const
{
	return SVector3f(x * f, y * f, z * f);
}

inline const SVector3f SVector3f::operator/(float f) const
{
	float r = 1.0f / f;
	return SVector3f(x * r, y * r, z * r);
}

inline const SVector3f SVector3f::operator+(const SVector3f& v) const
{
	return SVector3f(x + v.x, y + v.y, z + v.z);
}

inline const SVector3f SVector3f::operator-(const SVector3f& v) const
{
	return SVector3f(x - v.x, y - v.y, z - v.z);
}

inline const SVector3f SVector3f::operator*(const SVector3f& v) const
{
	return SVector3f(x * v.x, y * v.y, z * v.z);
}

inline const SVector3f operator*(float f, const SVector3f& v)
{
	return SVector3f(v.x * f, v.y * f, v.z * f);
}

inline SVector3f& SVector3f::operator*=(float f)
{
	x *= f; y *= f; z *= f;
	return *this;
}

inline SVector3f& SVector3f::operator/=(float f)
{
	float r = 1.0f / f;
	x *= r; y *= r; z *= r;
	return *this;
}

inline SVector3f& SVector3f::operator+=(const SVector3f& v)
{
	x += v.x; y += v.y; z += v.z;
	return *this;
}

inline SVector3f& SVector3f::operator-=(const SVector3f& v)
{
	x -= v.x; y -= v.y; z -= v.z;
	return *this;
}

inline float SVector3f::LengthSquared() const
{
	return x * x + y * y;
}
