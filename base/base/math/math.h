
#pragma once

struct SVector3f;

inline const int Maxi(const int a, const int b)
{
	return (a < b) ? b : a;
}

inline const int Mini(const int a, const int b)
{
	return (a < b) ? a : b;
}

inline const int Clampi(const int val, const int min, const int max)
{
	int retval = Maxi(val, min);
	retval = Mini(retval, max);
	return retval;
}

inline const float Lerpf(const float t, const float a, const float b) 
{ 
	return a + t * (b - a); 
}

inline const float Select(const float test, const float a, const float b)
{
	return (test > 0) ? a : b;
}

inline const float Maxf(const float a, const float b)
{
	return Select(a - b, a, b);
}

inline const float Minf(const float a, const float b)
{
	return Select(a - b, b, a);
}

bool SphereIntersectsAABB(const SVector3f& min, const SVector3f& max, const SVector3f& center, float radius);

