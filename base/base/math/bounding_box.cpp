

#include <base/math/bounding_box.h>
#include <base/defines.h>

void SBBox::FromSphere(const SVector3f& pos, float radius)
{
}
//______________________________________________________________________________

float SBBox::Width() const
{
	return max.x - min.x;
}
//______________________________________________________________________________

float SBBox::Depth() const
{
	return max.z - min.z;
}
//______________________________________________________________________________

void SBBox::Reset()
{
	min.x = min.y = min.z = FLT_MAX;
	max.x = max.y = max.z = -FLT_MAX;
}
//______________________________________________________________________________

void SBBox::Union(const SVector3f& pos)
{
	min.x = Minf(min.x, pos.x);
	min.y = Minf(min.y, pos.y);
	min.z = Minf(min.z, pos.z);

	max.x = Maxf(max.x, pos.x);
	max.y = Maxf(max.y, pos.y);
	max.z = Maxf(max.z, pos.z);
}
//______________________________________________________________________________

void SBBox::Expand(float x, float y, float z)
{
	min.x -= x;
	min.y -= y;
	min.z -= z;

	max.x += x;
	max.y += y;
	max.z += z;
}
//______________________________________________________________________________
