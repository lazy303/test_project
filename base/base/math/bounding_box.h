
#pragma once

#include <base/math/vector.h>

//Simple bounding box
struct SBBox
{
	SVector3f min;
	SVector3f max;

	void FromSphere(const SVector3f& pos, float radius); //Initialize the bounding box to contain a sphere

	float Width() const;
	float Depth() const;

	void Reset();							//Reset to corners of the bounding box to invalid values
	void Union(const SVector3f& pos);		//Expand bounding box to it contains pos
	void Expand(float x, float y, float z);	//Expand the bounding box in all directions
};
