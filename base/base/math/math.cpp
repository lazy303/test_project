
#include <base/math/math.h>
#include <base/math/vector.h>

bool SphereIntersectsAABB(const SVector3f& min, const SVector3f& max, const SVector3f& center, float radius)
{
	float temp;
	const float sqr_radius = radius * radius;	
	float dmin = 0.f;

	for (int i = 0; i < 3; i++)
	{
		if (center[i] < min[i])
		{
			temp = center[i] - min[i];
			dmin += temp * temp;
		}
		else if (center[i] > max[i])
		{
			temp = center[i] - max[i];
			dmin += temp * temp;
		}
	}

	if (dmin <= sqr_radius)
		return true;

	return false;
}
//______________________________________________________________________________
