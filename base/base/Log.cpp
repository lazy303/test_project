
#include "Log.h"

#include <windows.h>
#include <stdio.h>


void NLog::Print(const char* text, ...)
{
	va_list ap;
	va_start(ap, text);

	char buffer[1024] = {};
	_vsnprintf_s(buffer, sizeof(buffer), text, ap);

	va_end(ap);

	OutputDebugString(buffer);
}