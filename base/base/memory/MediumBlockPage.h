//-------------------
// CMediumBlockPage.h
// Jason Hughes
// Copyright 2010
// Steel Penny Games
//-------------------

#ifndef FMMMEDIUMBLOCKPAGE_H
#define FMMMEDIUMBLOCKPAGE_H

//-------------------

#include <Base/Misc/Assert.h>
#include "MyTypes.h"

//-------------------
// The basic layout of a medium block page is different because it accommodates larger, variable-sized allocations.
// It has a bit mask to identify which blocks (in equal-sized intervals specified by granularity) are free, plus
// another bit mask that indicates the last block in an allocation, so that we can tell how long allocations are when
// we're only given a starting pointer during the Free operation.
// [start of page]
// ...allocations...
// [AllocEnd Bitmask]
// [Free Bitmask]
// [Tail]
// [end of page]
//-------------------

template <uint TPageSize, uint TGranularity>
class CMediumBlockPage
{
public:
	void Initialize(void);
	
	// When an allocation occurs, we push the page it allocated from to the head. 
	// Generally this will cause allocations to be close together in time and space,
	// but also open space tends to flock together and get used together with fewer traversals.
	void* Alloc(uint sz, uint* alloc_size);

	// alloc_size is filled out with the number of bytes the allocation ACTUALLY took.
	// This returns true if the entire page is freed now.  That indicates the system should release the page back to the OSAPI.
	bool Free(void* ptr, uint* alloc_size);
	
	// When a new page is added by the system, this function manages the pointers.
	void InsertNewHeadPage(CMediumBlockPage** head_ptr);

	// When a page needs to be alloc/freed from, we remove it from the list first since it often will change lists
	void RemoveFromList(CMediumBlockPage** head_ptr);

	// fast accessor for the number of bits in the big block
	uint GetBigBlockSize(void) { return GetPageTail()->m_BigBlockSize; }
	
	// This makes sure everything checks out internally.
	void SanityCheck(void);

	// Debug functions
	void DebugOuputAllocationInfo() const;
 
private:
	// This struct situated as the very last few bytes in the allocated page.
	struct Tail
	{
		uint                m_BigBlockIndex; // index in blocks, not bytes
		uint                m_BigBlockSize;  // counted in blocks, not bytes
		uint                m_FreeBits;
		CMediumBlockPage* m_Next;
		CMediumBlockPage* m_Prev;
	};

	enum
	{
		kIdealNumGranules = (TPageSize+TGranularity-1)/TGranularity,  // this is only used as an interim for computation
		kTotalTailSize = (kIdealNumGranules+31)/32*4 * 2 + sizeof(Tail),  // there are TWO bitmasks (rounded to an even 32-bits), so count it twice.  Also only used as an interim.

		kNumGranules = (TPageSize - kTotalTailSize) / TGranularity,
		kBitmaskSize = (kNumGranules+31)/32*4,  // size in bytes.  round up to an even 32 bits for each mask, for performance	
		kTailOffset = TPageSize - sizeof(Tail),		
		kStartFreeOffset = kTailOffset - kBitmaskSize,		
		kStartAllocFreeOffset = kTailOffset - kBitmaskSize - kBitmaskSize,
	};
	
	// shortcuts to clean up a bunch of messy code
	Tail* GetPageTail     (void) const { return (Tail*)((char *)this + kTailOffset); }	
	void* GetStartFree    (void) const { return (char *)this + kStartFreeOffset; }
	void* GetStartAllocEnd(void) const { return (char *)this + kStartAllocFreeOffset; }	

	// This updates the tail so it has correct information about the big block size.
	// We have to do this after every alloc and free.
	void UpdateBigBlock(void);
};



template <uint TPageSize, uint TGranularity>
void CMediumBlockPage<TPageSize, TGranularity>::Initialize(void)
{
	Tail* tail = GetPageTail();

	// mark no bits in the end-of-allocation mask
	SetBitRangeTo0(GetStartAllocEnd(), 0, kNumGranules-1);

	// mark everything as free
	SetBitRangeTo1(GetStartFree(), 0, kNumGranules-1);

	// init the tail
	tail->m_BigBlockIndex = 0;            // big block in a new page is at the start of the page	
	tail->m_BigBlockSize = kNumGranules;
	tail->m_FreeBits = kNumGranules;      // tracks exactly how many granules are free within the page.
	tail->m_Next = NULL;
	tail->m_Prev = NULL;
}
//______________________________________________________________________________________

template <uint TPageSize, uint TGranularity>
void* CMediumBlockPage<TPageSize, TGranularity>::Alloc(uint sz, uint* alloc_size)
{
	// The algorithm for allocation is pretty straightforward.
	// 1. Compute how many blocks this allocation requires.
	// 2. Scan forward through pages until we find one with at least that many free.
	// 3. Scan the bits in that page and see if there's a block of bits that is long enough.
	// 4. If so, mark those bits as zero, reduce the free bits count, move that page to the head of the list, and return the pointer.
	// 5. If not, go to #2 and continue the scan for good pages.
	uint const alloc_bits_required = (sz + TGranularity - 1) / TGranularity;
	
	Tail* tail = GetPageTail();
	ASSERT(tail->m_FreeBits >= alloc_bits_required);
	ASSERT(tail->m_BigBlockSize >= alloc_bits_required);
	
	void* bitmask_start_free     = GetStartFree();	
	void* bitmask_start_alloc_end = GetStartAllocEnd();

	// since we are always maintaining the location of the big block, and we know that the CMemoryManager internally manages it so that
	// we always are allocating from a page whose big block is as close-fitting to the allocation as possible, we will simply
	// allocate from the big block directly and then recompute it.
	void* ptr = (char *)this + tail->m_BigBlockIndex * TGranularity;

	// mark the blocks as being ALLOCATED now
	SetBitRangeTo0(bitmask_start_free, tail->m_BigBlockIndex, tail->m_BigBlockIndex+alloc_bits_required-1);

	// mark the end of the allocation
	SetBitTo1(bitmask_start_alloc_end, tail->m_BigBlockIndex+alloc_bits_required-1);

	// now, figure out how big the bigblock on this page is now that we've taken a bite out of it
	UpdateBigBlock();
	tail->m_FreeBits -= alloc_bits_required;
	
	*alloc_size = alloc_bits_required * TGranularity;

	return ptr;
}
//______________________________________________________________________________________

template <uint TPageSize, uint TGranularity>
bool CMediumBlockPage<TPageSize, TGranularity>::Free(void* ptr, uint* alloc_size)
{
	ASSERT((((uint64)ptr - (uint64)this) % TGranularity) == 0);  // if this is false, it means we're trying to delete some random address in a page.

	// Freeing an object is as simple as walking a bit mask and marking allocated granules as free, until we find the end of the alloc in the AllocEnd mask.
	// We can also tell if there is corruption of some sort by asserting that bits are marked allocated when they should be.
	uint32 const starting_bit = (uint32)((uint64)ptr - (uint64)this) / TGranularity;

	Tail* tail						= GetPageTail();
	void* bitmask_start_free		= GetStartFree();	
	void* bitmask_start_alloc_end	= GetStartAllocEnd();

	// find the end of the allocation by scanning the bitmask for a 1 bit
	uint const end_of_alloc = FindFirst1BitInRange(bitmask_start_alloc_end, starting_bit, kNumGranules);
	ASSERT(end_of_alloc<kNumGranules);  // never found the end of the allocation.  That means something is busted.

	// mark the region of memory as free
	SetBitRangeTo1(bitmask_start_free, starting_bit, end_of_alloc);

	// mark the end-of-allocation to false again
	SetBitTo0(bitmask_start_alloc_end, end_of_alloc);

	// update the free bit count
	tail->m_FreeBits += end_of_alloc - starting_bit + 1;
	ASSERT(tail->m_FreeBits <= kNumGranules);  // if this exceeds the expected number of granules, we've got an accounting problem

	// return the size that this allocation took up
	*alloc_size = (end_of_alloc - starting_bit + 1) * TGranularity;

	// now that we've added some memory back to the page, maybe the bigblock is larger now?
	UpdateBigBlock();

	return (tail->m_FreeBits == kNumGranules);
}
//______________________________________________________________________________________

template <uint TPageSize, uint TGranularity>
void CMediumBlockPage<TPageSize, TGranularity>::UpdateBigBlock(void)
{
	Tail* tail             = GetPageTail();
	void* bitmask_start_free = GetStartFree();

	// note, as biggest block gets bigger, it becomes less important to check every last 
	// bit because beyond some point, no block CAN be bigger than what we already have.
	uint biggest_block = 0;
	uint biggest_block_index = 0;
	for (uint bit=0; bit<kNumGranules - biggest_block;)  
	{
		// scan forward to find the first free bit, then scan to find where the next allocated bit is
		uint const one_bit_index = FindFirst1BitInRange(bitmask_start_free, bit, kNumGranules-1);	
		if (one_bit_index==kNumGranules)  // no more free space
		{
			break;
		}

		// found an allocated region, so let's scan for the end of it
		uint const zero_bit_index = FindFirst0BitInRange(bitmask_start_free, one_bit_index+1, kNumGranules-1);
		uint const current_block_length = zero_bit_index - one_bit_index;
		if (current_block_length > biggest_block)
		{
			biggest_block      = current_block_length;
			biggest_block_index = one_bit_index;
		}

		bit = zero_bit_index + 1;
	}

	tail->m_BigBlockIndex = biggest_block_index;  // update our records
	tail->m_BigBlockSize  = biggest_block;
}
//______________________________________________________________________________________

template <uint TPageSize, uint TGranularity>
void CMediumBlockPage<TPageSize, TGranularity>::RemoveFromList(CMediumBlockPage** head_ptr)
{
	// very simply, insert this ahead of the head page.
	Tail* tail = GetPageTail();
	if (*head_ptr == this)
	{
		*head_ptr = tail->m_Next;  // fixup the head of the list, in case it was us
	}	
	if (tail->m_Prev)
	{
		Tail* prevTail = tail->m_Prev->GetPageTail();
		prevTail->m_Next = tail->m_Next;
	}
	if (tail->m_Next)
	{
		Tail* nextTail = tail->m_Next->GetPageTail();
		nextTail->m_Prev = tail->m_Prev;	
	}
	tail->m_Next = NULL;
	tail->m_Prev = NULL;
}
//______________________________________________________________________________________

template <uint TPageSize, uint TGranularity>
void CMediumBlockPage<TPageSize, TGranularity>::InsertNewHeadPage(CMediumBlockPage** head_ptr)
{
	// insert this ahead of the head page.
	Tail* tail = GetPageTail();
	ASSERT(tail->m_Next == NULL && tail->m_Prev == NULL);  // should only be doing this on a page not in a list already
	if (*head_ptr)
	{
		Tail* nextTail = (*head_ptr)->GetPageTail();
		nextTail->m_Prev = this;
	}
	tail->m_Next = *head_ptr;
	*head_ptr = this;
}
//______________________________________________________________________________________

template <uint TPageSize, uint TGranularity>
void CMediumBlockPage<TPageSize, TGranularity>::SanityCheck(void)
{
	CMediumBlockPage* current_page = this;
	while (current_page)
	{
		Tail* tail = current_page->GetPageTail();	
		ASSERT(tail->m_Prev==NULL || tail->m_Prev->GetPageTail()->m_Next==current_page);  // prev page must point to us	
		ASSERT(tail->m_Next==NULL || tail->m_Next->GetPageTail()->m_Prev==current_page);  // next page must point to us	
		ASSERT(tail->m_FreeBits >= tail->m_BigBlockSize);  // the free bit count should always be at least as high or higher than the big block size
	
		// we're going to walk ALL the free bits and see if the count matches the tail's count.
		void* bitmask_start_free = GetStartFree();
		uint bitsSet = 0;
		for (uint i = 0; i < kNumGranules; i++)
		{
			if (FindFirst1BitInRange(bitmask_start_free, i, i)==i)
				bitsSet++;
		}
		ASSERT(bitsSet==tail->m_FreeBits);
	
		// now, if we know how many bits are FREE, we can also tell what the upper limit of end-of-allocations there should be (numGranules - freeBits).
		void* bitmask_start_alloc_end = GetStartAllocEnd();		
		uint alloc_bits = 0;
		for (uint i = 0; i < kNumGranules; i++)
		{
			if (FindFirst1BitInRange(bitmask_start_alloc_end, i, i)==i)
			{
				alloc_bits++;
			}
		}
		ASSERT(alloc_bits <= kNumGranules - tail->m_FreeBits);
		current_page = tail->m_Next;  // iterate to the next page in the list and check it
	}
}
//______________________________________________________________________________________

template <uint TPageSize, uint TGranularity>
void CMediumBlockPage<TPageSize, TGranularity>::DebugOuputAllocationInfo() const
{
	uint8* bitmask_start_free	= static_cast<uint8*>(GetStartFree());
	void* bitmask_start_alloc_end		= GetStartAllocEnd();

	char memory[32];
	char buffer[512];

	for (uint block = 0; block < kNumGranules;)
	{
		for (uint b = 0; b < 8;)
		{
			if (block >= kNumGranules)
				break;			

			uint8 bit = 1 << b;
			if ((*bitmask_start_free & bit) == 0)
			{
				char* ptr = (char*)this + block * TGranularity;

				// find the end of the allocation by scanning the bitmask for a 1 bit
				uint const end_of_alloc = FindFirst1BitInRange(bitmask_start_alloc_end, block, kNumGranules);
				ASSERT(end_of_alloc<kNumGranules);  // never found the end of the allocation.  That means something is busted.

				uint const alloc_block_count = end_of_alloc - block + 1;
				uint const alloc_size = (end_of_alloc - block + 1) * TGranularity;

				memcpy(memory, ptr, 32);

				//Visualize 0 as a space
				for (int j = 0; j < 32; ++j)
					if (memory[j] == 0)
						memory[j] = ' ';

				memory[31] = 0;
				sprintf_s(buffer, 512, "{%d} Medium block at: %llX, %d bytes long.\nData: <%s>", block, (uint64)ptr, alloc_size, memory);
				OutputDebugString(buffer);
				for (int j = 0; j < 32; ++j)
				{
					uint tmp = ptr[j] & 0xFF;
					sprintf_s(buffer, 512, " %X", tmp);
					OutputDebugString(buffer);
				}				
				OutputDebugString("\n");

				block += alloc_block_count;
				bitmask_start_free += (b + alloc_block_count) / 8;
				b = block % 8;
			}
			else
			{
				++block;
				++b;
			}
		}

		bitmask_start_free++;
	}
}
//______________________________________________________________________________________

#endif
