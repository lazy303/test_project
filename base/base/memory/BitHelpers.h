//-------------------
// BitHelpers.h
// Jason Hughes
// Copyright 2010
// Steel Penny Games
//-------------------

#pragma once

//-------------------
#include <Base/Misc/Assert.h>
#include <Base/misc/Types.h>
#include "MyTypes.h"

//-------------------
// FindSetBitInRange will return the first bit in the specified range that is on, or will return bit_end+1.
uint FindFirst1BitInRange(void const* root, uint bit_start, uint bit_end);
uint FindFirst0BitInRange(void const* root, uint bit_start, uint bit_end);

inline uint FindFirstSetBit(uint data);

// These functions are specially optimized to work in 32-bits at all times, with good memory alignment performance.
// NOT endian-safe at the moment.  However, if you only use these functions to access bits rather than calculating
// them directly, it's not important.
void SetBitRangeTo1(void* root, uint bit_start, uint bit_end);
void SetBitRangeTo0(void* root, uint bit_start, uint bit_end);

//-------------------
// These always work in 32-bit aligned memory, specifically so we don't have to worry about endianness when porting.
// If all memory is 32-bit aligned, and you access it as 32-bits always, the byte order doesn't really matter.
// It does matter if you treat the memory as bytes sometimes and 32-bit other times.
inline void SetBitTo1(void* root, uint bit_start)
{
	// we require root to be 4-byte address aligned, for memory performance reasons.
	ASSERT(((size_t)root & 3)==0);

	// Now, we operate entirely in 32-bit space. (which means Endian-ness matters)
	unsigned* current_word = (unsigned*)root + bit_start/32;
	*current_word |= 1<<(bit_start & 31);
}
//______________________________________________________________________________________

inline void SetBitTo0(void* root, uint bit_start)
{
	// we require root to be 4-byte address aligned, for memory performance reasons.
	ASSERT(((size_t)root & 3)==0);

	// Now, we operate entirely in 32-bit space. (which means Endian-ness matters)
	unsigned* current_word = (unsigned*)root + bit_start/32;
	*current_word &= ~(1<<(bit_start & 31));
}
//______________________________________________________________________________________
