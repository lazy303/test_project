//-------------------
// OSAPIFastForWindows.h
// Jason Hughes
// Copyright 2010
// Steel Penny Games
//-------------------

#ifndef OSAPIFASTFORWINDOWS_H
#define OSAPIFASTFORWINDOWS_H

//-------------------

#include "MemoryManager.h"
#include "Mallocator.h"
#include <vector>
#define _SILENCE_STDEXT_HASH_DEPRECATION_WARNINGS
#include <hash_map>
#undef _SILENCE_STDEXT_HASH_DEPRECATION_WARNINGS
#include <algorithm>
#include <windows.h>

//-------------------
// This is a reimagined API for windows.  The idea is primarily to REMOVE, as much
// as possible, calls to the kernel to allocate pages of memory and to limit the
// amount of searching and scanning required to identify pointers as small/medium/large.
// To do this, we have to make some concessions about the flexibility of the memory manager.
class COSAPIFastForWindows
{
public:
	COSAPIFastForWindows(void);
	~COSAPIFastForWindows(void);

	// This is very important to be implemented in a fast way, so free is quick.
	void* IsSmallBlock (void* ptr) const;	
	void* IsMediumBlock(void* ptr) const;
	bool  IsLargeBlock (void* ptr) const;

	// these functions need to be overridden to change where and how 
	// the memory manager gets small pages.
	void* AllocSmallPage(void);
	void  FreeSmallPage (void* ptr);

	// these functions need to be overridden to change where and how
	// the memory manager gets medium pages.
	void* AllocMediumPage(void);
	void  FreeMediumPage (void* ptr);

	// these functions are called whenever the CMemoryManager needs a very large
	// single allocation.  Generally, in Windows this is better to just
	// map to VirtualAlloc, which guarantees no fragmentation but may
	// waste some memory between the end of the allocation and the end
	// of the physical memory page mapped to the address space.  Consoles
	// may direct this to a traditional linked list allocator or manually 
	// handle VirtualAlloc-like behavior by mapping memory blocks.
	void* AllocLarge(uint sz);
	
	// AllocSize is returned so the system can track how much memory is out in each type.
	void  FreeLarge (void* ptr, uint* alloc_size);	

	// definitions that the CMemoryManager will use
	enum
	{
		// This allocation scheme requires small and medium page size to be equal, since we just
		// use a bit to determine which type of page it was.
		kSmallPageSize  = 16*1024,
		kMediumPageSize = 16*1024,

		kGiantBlockSize = 256*1024*1024,  // 256mb of memory is the upper-limit of our allocator
		kTotalPages     = kGiantBlockSize / kSmallPageSize,
	};

	void* operator new(size_t size)
	{
		return _aligned_malloc(size, 16);
	}
	//______________________________________________________________________________________

	void operator delete(void* ptr)
	{
		return _aligned_free(ptr);
	}
	//______________________________________________________________________________________

	void DebugOuputAllocationInfo();

private:
	// The best way to make this fast is to allocate a ton of memory at the start and dole it out
	// in pages, just remembering which pages are small and which are medium.  Any address that is
	// not in the giant allocation range is clearly going to be large.
	void* m_GiantBlock;
	std::vector<bool, Mallocator<bool> > m_IsFreePage;	
	std::vector<bool, Mallocator<bool> > m_IsSmallPage;

	typedef stdext::hash_map<void*, uint, std::hash_compare<void*, std::less<void*> >, Mallocator<std::pair<void*, uint> > > my_hash_map;

	my_hash_map m_LargeAllocs;  // pair of pointers and sizes, which isn't fast but is easiest to implement for now

	// We fetch this from the OS, and make sure it's what our page sizes are set to.
	uint m_NaturalPageSize;

	// this is a simple cache that prevents us from searching most of the time when looking for a free page.
	uint64 m_LastPageFreed;
};




COSAPIFastForWindows::COSAPIFastForWindows(void)
	: m_IsFreePage(kTotalPages, true), m_IsSmallPage(kTotalPages, false)
{
	SYSTEM_INFO info;
	GetSystemInfo(&info);
	m_NaturalPageSize = info.dwPageSize;

	// If this triggers, you are running in an inoptimal way and should change your 
	// size to match or be a multiple of natural page size of the OS.
	ASSERT(kSmallPageSize % m_NaturalPageSize == 0 && kMediumPageSize % m_NaturalPageSize == 0);	

	// page sizes must be identical to use this allocation policy
	ASSERT(kSmallPageSize == kMediumPageSize);

	m_GiantBlock = VirtualAlloc(NULL, kGiantBlockSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);

	uint32 error = 0;
	if (m_GiantBlock == nullptr)
	{
		error = GetLastError();
	}	

	m_LastPageFreed = 0;
}
//______________________________________________________________________________________

COSAPIFastForWindows::~COSAPIFastForWindows(void)
{
	VirtualFree(m_GiantBlock, 0, MEM_RELEASE);
}
//______________________________________________________________________________________

void* COSAPIFastForWindows::IsSmallBlock(void* ptr) const
{
	// fail if it's not within the giant block of doom
	if ((uint64)ptr < (uint64)m_GiantBlock || (uint64)ptr >= (uint64)m_GiantBlock + kGiantBlockSize)
		return false;

	uint64 const page_index = ((uint64)ptr - (uint64)m_GiantBlock) / kSmallPageSize;
	ASSERT(!m_IsFreePage[page_index]);
	if (m_IsSmallPage[page_index])
	{
		return ((char*)m_GiantBlock + page_index * kSmallPageSize);
	}
	return NULL;  // no, it's a medium block
}
//______________________________________________________________________________________

void* COSAPIFastForWindows::IsMediumBlock(void* ptr) const
{
	// fail if it's not within the giant block of doom
	if ((uint64)ptr < (uint64)m_GiantBlock || (uint64)ptr >= (uint64)m_GiantBlock + kGiantBlockSize)
		return false;

	uint64 const page_index = ((uint64)ptr - (uint64)m_GiantBlock) / kSmallPageSize;
	ASSERT(!m_IsFreePage[page_index]);
	if (!m_IsSmallPage[page_index])
	{
		return ((char*)m_GiantBlock + page_index * kMediumPageSize);
	}
	return NULL;  // no, it's a medium block	
}
//______________________________________________________________________________________

bool COSAPIFastForWindows::IsLargeBlock(void* ptr) const
{
	// if it's not inside our block, it must be a large allocation
	if ((uint64)ptr < (uint64)m_GiantBlock || (uint64)ptr >= (uint64)m_GiantBlock + kGiantBlockSize)
		return true;
	return false;
}
//______________________________________________________________________________________

// these functions need to be overridden to change where and how 
// the memory manager gets small pages.
void* COSAPIFastForWindows::AllocSmallPage(void)
{
	// first iteration searches starting at the cached value
	for (uint64 page_index = m_LastPageFreed; page_index<kTotalPages; ++page_index)
	{
		if (m_IsFreePage[page_index])
		{
			m_IsFreePage[page_index] = false;
			m_IsSmallPage[page_index] = true;
			m_LastPageFreed = page_index + 1;
			return ((char *)m_GiantBlock + page_index * kSmallPageSize);
		}
	}

	// second half of loop starts at the beginning
	for (uint64 page_index = 0; page_index<m_LastPageFreed; ++page_index)
	{
		if (m_IsFreePage[page_index])
		{
			m_IsFreePage[page_index] = false;
			m_IsSmallPage[page_index] = true;
			m_LastPageFreed = page_index + 1;			
			return ((char *)m_GiantBlock + page_index * kSmallPageSize);
		}
	}

	ASSERT(false);  // major fail here... can't allocate any new pages
	return NULL;
}
//______________________________________________________________________________________

void COSAPIFastForWindows::FreeSmallPage(void* ptr)
{
	// fail if it's not within the giant block of doom
	ASSERT((uint64)ptr >= (uint64)m_GiantBlock && (uint64)ptr < (uint64)m_GiantBlock + kGiantBlockSize);
	ASSERT((((uint64)ptr - (uint64)m_GiantBlock) % kSmallPageSize) == 0);  // must be the start of a page address or something is wrong.

	uint64 const page_index = ((uint64)ptr - (uint64)m_GiantBlock) / kSmallPageSize;
	ASSERT(!m_IsFreePage[page_index]);
	ASSERT(m_IsSmallPage[page_index]);
	m_IsFreePage[page_index] = true;
	m_LastPageFreed = page_index;
}
//______________________________________________________________________________________

void* COSAPIFastForWindows::AllocMediumPage(void)
{
	// first iteration searches starting at the cached value
	for (uint64 page_index = m_LastPageFreed; page_index<kTotalPages; ++page_index)
	{
		if (m_IsFreePage[page_index])
		{
			m_IsFreePage[page_index] = false;
			m_IsSmallPage[page_index] = false;
			m_LastPageFreed = page_index + 1;			
			return ((char *)m_GiantBlock + page_index * kMediumPageSize);
		}
	}

	// second half of loop starts at the beginning
	for (uint64 page_index = 0; page_index<m_LastPageFreed; ++page_index)
	{
		if (m_IsFreePage[page_index])
		{
			m_IsFreePage[page_index] = false;
			m_IsSmallPage[page_index] = false;
			m_LastPageFreed = page_index + 1;			
			return ((char *)m_GiantBlock + page_index * kMediumPageSize);
		}
	}

	ASSERT(false);  // major fail here... can't allocate any new pages
	return NULL;
}
//______________________________________________________________________________________

void COSAPIFastForWindows::FreeMediumPage(void* ptr)
{
	// fail if it's not within the giant block of doom
	ASSERT((uint64)ptr >= (uint64)m_GiantBlock && (uint64)ptr < (uint64)m_GiantBlock + kGiantBlockSize);
	ASSERT((((uint64)ptr - (uint64)m_GiantBlock) % kMediumPageSize) == 0);  // must be the start of a page address or something is wrong.

	uint64 const page_index = ((uint64)ptr - (uint64)m_GiantBlock) / kMediumPageSize;
	ASSERT(!m_IsFreePage[page_index]);
	ASSERT(!m_IsSmallPage[page_index]);
	m_IsFreePage[page_index] = true;
	m_LastPageFreed = page_index;	
}
//______________________________________________________________________________________

void* COSAPIFastForWindows::AllocLarge(uint size)
{
//	void* addr = VirtualAlloc(NULL, sz, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	void* addr = malloc(size);
	m_LargeAllocs.insert(std::make_pair(addr, size));
	return addr;
}
//______________________________________________________________________________________

// AllocSize is returned so the system can track how much memory is out in each type.
void COSAPIFastForWindows::FreeLarge(void* ptr, uint* alloc_size)
{
	my_hash_map::iterator it = m_LargeAllocs.find(ptr);

	ASSERT(it != m_LargeAllocs.end());  // didn't find it, didn't free it
	if (it != m_LargeAllocs.end())
	{
		if (alloc_size)
			*alloc_size = it->second;

#if defined(RUIN_FREED_MEMORY)
		memset(ptr, FREED_MEMORY_PATTERN, it->second);
#endif	

//		VirtualFree(ptr, 0, MEM_RELEASE);
		free(ptr);

		m_LargeAllocs.erase(it);
		return;	
	}
}
//______________________________________________________________________________________

void COSAPIFastForWindows::DebugOuputAllocationInfo()
{
	my_hash_map::iterator it = m_LargeAllocs.begin();
	my_hash_map::iterator end_it = m_LargeAllocs.end();

	char memory[32];
	char buffer[512];

	for (int count = 0; it != end_it; ++it, ++count)
	{
		uint8* ptr = static_cast<uint8*>(it->first);
		const uint size = it->second;
		memcpy(memory, ptr, 32);

		//Visualize 0 as a space
		for (int j = 0; j < 32; ++j)
			if (memory[j] == 0)
				memory[j] = ' ';

		memory[31] = 0;
		sprintf_s(buffer, 512, "Addr: %llX size: %d mem:>\"%s\"<\n", (uint64)ptr, size, memory);
		OutputDebugStringA(buffer);

		sprintf_s(buffer, 512, "{%d} Large block at: %llX, %d bytes long.\nData: <%s>", count, (uint64)ptr, size, memory);
		OutputDebugStringA(buffer);
		for (int j = 0; j < 32; ++j)
		{
			uint tmp = ptr[j] & 0xFF;
			sprintf_s(buffer, 512, " %X", tmp);
			OutputDebugStringA(buffer);
		}				
		OutputDebugStringA("\n");
	}
}
//______________________________________________________________________________________

#endif
