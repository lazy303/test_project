//-------------------
// MemoryManager.h
// Jason Hughes
// Copyright 2010
// Steel Penny Games
//-------------------

#ifndef FMM_H
#define FMM_H

//-------------------

#include "MyTypes.h"
#include "SmallBlockPage.h"
#include "MediumBlockPage.h"
#include <Base/Misc/Assert.h>
#include <Base/math/Math.h>

#ifdef LZ_USE_MEMTRACE
#include <MemTrace/MemTrace.h>
#endif

//-------------------
// This is the interface to the Fast Memory Manager.  To change the 
// behavior of it, simply derive from it and override the virtual 
// methods that interact with the operating system's memory manager.
//
// The first three arguments define the crossover points between
// allocation types.  Particularly, minSmallAlloc is the minimum
// allocation size in the system.  These alloc sizes must be powers
// of 2 for allocation alignment to work correctly, but you
// can set them any way you like if that doesn't matter.  Cache 
// performance may suffer.
//
// SmallPageSize should be a natural-sized (for the OS/memory architecture)
// length like 4096 (for Win32).  Larger is fine, but only if you know 
// there will be tons of allocations at each interval so it's not wastefully
// reserved.  Also consider that the address of a pointer being freed can 
// ideally be able to identify whether it is a small block or not and if so,
// what page it came from.  Some of that can be done entirely with bit masking
// if all small blocks are allocated 4k aligned and 4k long, for instance.
//
// MediumPageSize has similar issues to the SmallPageSize, only with bigger 
// pages, and larger allocation granularity.  MediumPageSize should be larger
// than kMinLargeAlloc, otherwise the largest medium allocation cannot fit in
// a brand new Medium Page.  That'd be an infinite loop until you run out of RAM.
//
// OSAPI is a traits class that handles allocating pages for the memory manager.
// That's an important job, and one that can be done many ways.

#ifdef _DEBUG
#define RUIN_ALLOCATED_MEMORY
#define RUIN_FREED_MEMORY
#endif

#define FREED_MEMORY_PATTERN 0xfefe
#define ALLOCATED_MEMORY_PATTERN 0xcdcd

struct SMemoryData
{
	uint m_Small;
	uint m_Medium;
	uint m_Large;
};

inline const uint32 Maxu(const uint32 a, const uint32 b)
{
	return (a < b) ? b : a;
}

template <uint kMinSmallAlloc, uint kSmallAllocStepSize, uint kMinMediumAlloc, uint kMediumAllocStepSize, uint kMinLargeAlloc, class OSAPI>
class CMemoryManager
{
public:

	CMemoryManager(bool assert_on_memory_leak = true);
	~CMemoryManager();

	void* Alloc(uint size);
	bool  Free(void* ptr);  // returns false if this was not our pointer!

	void  SanityCheck(void);  // This is SLOW.  Don't use it unless you're tracking a bug of some sort.

	void GetAllocationInfo(SMemoryData& allocations, SMemoryData& allocated, SMemoryData& top_allocated);

private:
	// This declares how many small pages we will have, stepping in 
	// kSmallAllocStepSize intervals
	enum 
	{
		kSmallPageSize  = OSAPI::kSmallPageSize,
		kMediumPageSize = OSAPI::kMediumPageSize,

		// this is how many different allocation sizes can be requested from small blocks
		kNumSmallPages  = (kMinMediumAlloc-kMinSmallAlloc + kSmallAllocStepSize - 1)/kSmallAllocStepSize,  

		// this is how many different blocks in a row are free on a completely new page
		kNumMediumPages = kMediumPageSize/kMediumAllocStepSize,  
	};
	typedef CMediumBlockPage<kMediumPageSize, kMediumAllocStepSize> TMediumBlockPage;	
	typedef CSmallBlockPage<kSmallPageSize>                         TSmallBlockPage;

	// This is effectively a hash-table where each page pointer is a
	// linked list of pages full of BLOCKS, serving as allocation pools.
	// They may be NULL if no page is currently available serving that
	// sized allocation.  NB: all allocations on a single page are
	// the same size, so pages do not move between lists.
	TSmallBlockPage* m_SmallPages[kNumSmallPages];		

	// these are pages that are simply out of space, so we don't want to search them.
	// When something frees up in one of these pages, it gets moved to the m_SmallPages lists.
	TSmallBlockPage* m_SmallPagesFull[kNumSmallPages];  

	// This is effectively a hash-table where these are effectively
	// heads of linked-lists of pages that guarantee there is a big-block
	// of the given size on each page in that list.  Obviously, medium
	// pages serve any kind of allocation size within the allowable range
	// and the big-block changes, so one page may move from the largest
	// (kNumMediumPages-1) down to the smallest (0 free)
	TMediumBlockPage* m_MediumPages[kNumMediumPages];

	// This is a simple bitmask that tells whether or not the corresponding entry
	// in m_MediumPages has a pointer or is null.  This way, we can do a really fast
	// bit scan to search for a pointer rather than read in a bunch of pointers in a loop.
	uint m_MediumPagesBitmask[(kNumMediumPages + 31) / 32];

	// This is our interface to the OS's memory, and also the page sizes.  The
	// interface MUST implement the following class definition:
	OSAPI*	m_OSAPI;	

	SMemoryData m_Allocations;
	SMemoryData m_Allocated;
	SMemoryData m_TopAllocated;

	bool	m_AssertOnMemoryLeak;

	CRITICAL_SECTION	m_MemoryMutex;

#ifdef LZ_USE_MEMTRACE
	MemTrace::HeapId m_MemTraceHeapId;
#endif
};



// Implement the template.  Remember, the main reason for creating this as a template is 
// to PREVENT reading data from all over the place, rather we embed it directly in the 
// code cache.
template <uint kMinSmallAlloc, uint kSmallAllocStepSize, uint kMinMediumAlloc, uint kMediumAllocStepSize, uint kMinLargeAlloc, class OSAPI>
CMemoryManager<kMinSmallAlloc, kSmallAllocStepSize, kMinMediumAlloc, kMediumAllocStepSize, kMinLargeAlloc, OSAPI>::CMemoryManager(bool assert_on_memory_leak)
	: m_OSAPI(nullptr)
	, m_AssertOnMemoryLeak(assert_on_memory_leak)
{
	InitializeCriticalSection(&m_MemoryMutex);

	for (uint i = 0; i < kNumSmallPages; i++)
	{
		m_SmallPages[i] = NULL;
		m_SmallPagesFull[i] = NULL;
	}
	for (uint i = 0; i < kNumMediumPages; i++)
	{
		m_MediumPages[i] = NULL;
	}	

	// mark all the pages as missing
	SetBitRangeTo0(&m_MediumPagesBitmask, 0, kNumMediumPages-1);

	m_Allocated.m_Small = 0;
	m_Allocated.m_Medium = 0;
	m_Allocated.m_Large = 0;
	m_Allocations.m_Small = 0;
	m_Allocations.m_Medium = 0;
	m_Allocations.m_Large = 0;
	m_TopAllocated.m_Small = 0;
	m_TopAllocated.m_Medium = 0;
	m_TopAllocated.m_Large = 0;

	m_OSAPI = new OSAPI;

#ifdef LZ_USE_MEMTRACE
	m_MemTraceHeapId = MemTrace::HeapCreate("lz allocator");
#endif
}
//______________________________________________________________________________________

template <uint kMinSmallAlloc, uint kSmallAllocStepSize, uint kMinMediumAlloc, uint kMediumAllocStepSize, uint kMinLargeAlloc, class OSAPI>
CMemoryManager<kMinSmallAlloc, kSmallAllocStepSize, kMinMediumAlloc, kMediumAllocStepSize, kMinLargeAlloc, OSAPI>::~CMemoryManager(void)
{
	char allocation_info[1024];
	sprintf_s(allocation_info, 1024, "\nSmall Memory Allocated : %d, #allocations : %d\nMedium Memory Allocated : %d, #allocations : %d\nLarge Memory Allocated : %d, #allocations : %d\n\n", 
		m_Allocated.m_Small, 
		m_Allocations.m_Small,
		m_Allocated.m_Medium,
		m_Allocations.m_Medium,
		m_Allocated.m_Large,
		m_Allocations.m_Large);

	OutputDebugString(allocation_info);


	bool leak_detected = false;

	// if any of these asserts trip, it's because there's memory leaks
	for (uint i = 0; i < kNumSmallPages; i++)
	{	
		if (m_SmallPages[i] != nullptr)
		{
			m_SmallPages[i]->SanityCheck();
			m_SmallPages[i]->DebugOuputAllocationInfo();
		}
		if (m_SmallPagesFull[i] != nullptr)
		{
			m_SmallPagesFull[i]->SanityCheck();
			m_SmallPagesFull[i]->DebugOuputAllocationInfo();
		}

		if (m_SmallPages[i] != nullptr || m_SmallPagesFull[i] != nullptr)
			leak_detected = true;
	}
	
	for (uint i = 0; i < kNumMediumPages; i++)
	{
		if (m_MediumPages[i] != nullptr)
		{
			m_MediumPages[i]->SanityCheck();
			m_MediumPages[i]->DebugOuputAllocationInfo();
		}

		if (m_MediumPages[i] != nullptr)
			leak_detected = true;
	}
	// check to see if the bitmask is correctly tracked
	if (FindFirst1BitInRange(&m_MediumPagesBitmask, 0, kNumMediumPages-1) != kNumMediumPages)
		leak_detected = true;

	m_OSAPI->DebugOuputAllocationInfo();

	if (leak_detected && m_AssertOnMemoryLeak)
	{
		if (IsDebuggerPresent())
		{
			ASSERT(!"MEMORY LEAK DETECTED");
		}
	}

	delete m_OSAPI;
	m_OSAPI = nullptr;

	DeleteCriticalSection(&m_MemoryMutex);

#ifdef LZ_USE_MEMTRACE
	MemTrace::HeapDestroy(m_MemTraceHeapId);
#endif
}
//______________________________________________________________________________________

template <uint kMinSmallAlloc, uint kSmallAllocStepSize, uint kMinMediumAlloc, uint kMediumAllocStepSize, uint kMinLargeAlloc, class OSAPI>
void* CMemoryManager<kMinSmallAlloc, kSmallAllocStepSize, kMinMediumAlloc, kMediumAllocStepSize, kMinLargeAlloc, OSAPI>::Alloc(uint sz)
{
	EnterCriticalSection(&m_MemoryMutex);

	ASSERT(sz < 128 * 1024 * 1024);

	sz = (sz < kMinSmallAlloc ? kMinSmallAlloc : sz);  // clamp to at least min alloc size

	uint const small_list_index = (sz - kMinSmallAlloc + kSmallAllocStepSize - 1) / kSmallAllocStepSize;  // round up to next step size, if necessary
	uint const small_size = small_list_index * kSmallAllocStepSize + kMinSmallAlloc;
	
	if (small_size < kMinMediumAlloc)  // small allocation
	{
		TSmallBlockPage* page = m_SmallPages[small_list_index];
		if (!page)
		{
			// allocate a new page, initialize it and assign it to the head of the list
			page = (TSmallBlockPage*)m_OSAPI->AllocSmallPage();
			page->Initialize(small_size);
			m_SmallPages[small_list_index] = page;
		}

		// attempt allocating a block from the correctly-sized linked list of small block pages.  It may fail if all of them are full.
		bool is_full = false;
		void* ret = page->Alloc(small_size, &is_full);

		// move this page if it has no free space left
		if (is_full)
		{
			page->RemoveFromList(&m_SmallPages[small_list_index]);
			page->InsertNewHeadPage(&m_SmallPagesFull[small_list_index]);
		}

		m_Allocated.m_Small += small_size;
		m_Allocations.m_Small++;
		m_TopAllocated.m_Small = Maxu(m_TopAllocated.m_Small, m_Allocated.m_Small);

		LeaveCriticalSection(&m_MemoryMutex);

#if defined(RUIN_ALLOCATED_MEMORY)
		memset(ret, ALLOCATED_MEMORY_PATTERN, small_size);  // mark new memory as uninitialized
#endif

#ifdef LZ_USE_MEMTRACE
		MemTrace::HeapAllocate(m_MemTraceHeapId, ret, sz);
#endif
		
		return ret;
	}
	else if (sz < kMinLargeAlloc)  // medium sized allocation
	{
		// round up to an even granularity in medium pages.  Special case is if sz was less than the minimum allocation size,
		// because the rounded-up small_size may not have the same modulus as kMinMediumAlloc...
		uint const medium_size = sz < kMinMediumAlloc ? kMinMediumAlloc : ((sz - kMinMediumAlloc + kMediumAllocStepSize - 1) / kMediumAllocStepSize) * kMediumAllocStepSize + kMinMediumAlloc;
		uint const contiguous_blocks_required = (medium_size + kMediumAllocStepSize - 1) / kMediumAllocStepSize;  // round up since we might need a partial block
		ASSERT(contiguous_blocks_required<kNumMediumPages);  // make sure we can use this to index our table of pages
		ASSERT(contiguous_blocks_required>0);

		// so, if we can't find EXACTLY the big-block size we want, that's ok.  We can always take a larger one.
		TMediumBlockPage* medium_page = NULL;
		uint const page_index = FindFirst1BitInRange(&m_MediumPagesBitmask, contiguous_blocks_required, kNumMediumPages-1);	
		if (page_index==kNumMediumPages)  // no page is available with enough space for our needs, so create one
		{
			medium_page = (TMediumBlockPage*)m_OSAPI->AllocMediumPage();
			ASSERT(medium_page != nullptr);
			medium_page->Initialize();		
		}
		else
		{
			medium_page = m_MediumPages[page_index];
			ASSERT(medium_page!=NULL);
			medium_page->RemoveFromList(&m_MediumPages[page_index]);

			if (!m_MediumPages[page_index])  // if this was the last page in the list, we have to update the bit to reflect that
			{
				SetBitTo0(&m_MediumPagesBitmask, page_index);
			}
		}

		// allocate memory from the page first
		uint alloc_size = 0;
		void* ret = medium_page->Alloc(medium_size, &alloc_size);

		// next, figure out how big the big-block is on this page and add it to the right list.
		uint const big_block_bits_in_a_row = medium_page->GetBigBlockSize();
		ASSERT(big_block_bits_in_a_row < kNumMediumPages);  // again, make sure this makes sense.
		if (!m_MediumPages[big_block_bits_in_a_row])  // if this page list is just now acquiring a page, set its bit to reflect that
		{
			SetBitTo1(&m_MediumPagesBitmask, big_block_bits_in_a_row);
		}
		medium_page->InsertNewHeadPage(&m_MediumPages[big_block_bits_in_a_row]);

		m_Allocated.m_Medium += alloc_size;
		m_Allocations.m_Medium++;
		m_TopAllocated.m_Medium = Maxu(m_TopAllocated.m_Medium, m_Allocated.m_Medium);

		LeaveCriticalSection(&m_MemoryMutex);

#if defined(RUIN_ALLOCATED_MEMORY)
		memset(ret, ALLOCATED_MEMORY_PATTERN, alloc_size);  // mark new memory as uninitialized
#endif	

#ifdef LZ_USE_MEMTRACE
		MemTrace::HeapAllocate(m_MemTraceHeapId, ret, sz);
#endif

		return ret;
	}
	else  // large allocation
	{
		m_Allocated.m_Large += sz;
		m_Allocations.m_Large++;	
		m_TopAllocated.m_Large = Maxu(m_TopAllocated.m_Large, m_Allocated.m_Large);
		void* ret = m_OSAPI->AllocLarge(sz);

		ASSERT(ret != nullptr);

		LeaveCriticalSection(&m_MemoryMutex);

#if defined(RUIN_ALLOCATED_MEMORY)
		memset(ret, ALLOCATED_MEMORY_PATTERN, sz);  // mark new memory as uninitialized
#endif	

#ifdef LZ_USE_MEMTRACE
		MemTrace::HeapAllocate(m_MemTraceHeapId, ret, sz);
#endif

		return ret;
	}
}
//______________________________________________________________________________________

template <uint kMinSmallAlloc, uint kSmallAllocStepSize, uint kMinMediumAlloc, uint kMediumAllocStepSize, uint kMinLargeAlloc, class OSAPI>
bool CMemoryManager<kMinSmallAlloc, kSmallAllocStepSize, kMinMediumAlloc, kMediumAllocStepSize, kMinLargeAlloc, OSAPI>::Free(void* ptr)
{
	if (ptr == nullptr)
		return true;

	ASSERT((uint64)ptr != 0xfefefefefefefefe && "Memory has already been freed!");
	ASSERT((uint64)ptr != 0xcdcdcdcdcdcdcdcd && "Uninitialized memory!");

	EnterCriticalSection(&m_MemoryMutex);

	TSmallBlockPage* small_page = (TSmallBlockPage*)m_OSAPI->IsSmallBlock(ptr);
	if (small_page)
	{
		uint const block_size = small_page->GetBlockSize();
		uint const list_index = (block_size - kMinSmallAlloc) / kSmallAllocStepSize;  // figure out what list it's on

		// we need to 
		TSmallBlockPage* *page_head     = &m_SmallPages[list_index];		
		TSmallBlockPage* *page_head_full = &m_SmallPagesFull[list_index];
		ASSERT(*page_head!=NULL || *page_head_full!=NULL);  // there should definitely be SOME page in this list from which to free memory.

		m_Allocated.m_Small -= block_size;  // update tracking
		m_Allocations.m_Small--;

#if defined(RUIN_FREED_MEMORY)
		memset(ptr, FREED_MEMORY_PATTERN, block_size);  // mark free memory
#endif				
		// free the allocation.  if it's a completely empty page now, remove it from the list and release it back to the OS.
		bool was_full = false;
		bool is_now_empty = small_page->Free(ptr, &was_full);
		if (was_full)  // switch back to the the normal list that has memory available for allocation
		{
			small_page->RemoveFromList(page_head_full);
			small_page->InsertNewHeadPage(page_head);
		}
		if (is_now_empty)
		{
			small_page->RemoveFromList(page_head);
			m_OSAPI->FreeSmallPage(small_page);
		}

		LeaveCriticalSection(&m_MemoryMutex);

#ifdef LZ_USE_MEMTRACE
		MemTrace::HeapFree(m_MemTraceHeapId, ptr);
#endif

		return true;				
	}
	else
	{
		TMediumBlockPage* medium_page = (TMediumBlockPage*)m_OSAPI->IsMediumBlock(ptr);
		if (medium_page)
		{
			// we have to figure out how big the big block is, which then tells us which list the page is on.
			// Then, we remove it from that list, free the memory, measure the big block again, and either delete
			// the page (if it's completely free) or insert it at the head of the right list.
			uint const big_block_bits_in_a_row = medium_page->GetBigBlockSize();
			ASSERT(big_block_bits_in_a_row < kNumMediumPages);  // page cannot be completely free AND on a list

			// take this page out of its current page list
			medium_page->RemoveFromList(&m_MediumPages[big_block_bits_in_a_row]);
			if (!m_MediumPages[big_block_bits_in_a_row])  // clear the bit if this list is empty now
			{
				SetBitTo0(&m_MediumPagesBitmask, big_block_bits_in_a_row);			
			}

			// free the memory
			uint alloc_size = 0;
			if (medium_page->Free(ptr, &alloc_size))
			{
				// since the page is completely empty now, free the page
				m_OSAPI->FreeMediumPage(medium_page);
			}
			else  // otherwise insert it back at the head of the appropriate-sized list
			{
#if defined(RUIN_FREED_MEMORY)
				memset(ptr, FREED_MEMORY_PATTERN, alloc_size);  // mark free memory
#endif							
				uint const new_big_block_size = medium_page->GetBigBlockSize();

				if (!m_MediumPages[new_big_block_size])  // if this list is just getting a page, set the bit
				{
					SetBitTo1(&m_MediumPagesBitmask, new_big_block_size);			
				}
				medium_page->InsertNewHeadPage(&m_MediumPages[new_big_block_size]);
			}

#ifdef _DEBUG
			uint mem_allocated = m_Allocated.m_Medium;
#endif

			m_Allocated.m_Medium -= alloc_size;  // update general tracking
			m_Allocations.m_Medium--;

#ifdef _DEBUG
			ASSERT(m_Allocated.m_Medium < mem_allocated);
#endif
			
			LeaveCriticalSection(&m_MemoryMutex);

#ifdef LZ_USE_MEMTRACE
			MemTrace::HeapFree(m_MemTraceHeapId, ptr);
#endif

			return true;				
		}	
		else  // must be a large block
		{
			ASSERT(m_OSAPI->IsLargeBlock(ptr));  // if it's not this, we have to assume that someone allocated this pointer elsewhere and it's an error to free it here.
			uint alloc_size = 0;
			m_OSAPI->FreeLarge(ptr, &alloc_size); //Also Ruins freed memori if flag is set.
			
			m_Allocated.m_Large -= alloc_size;  // update tracking
			m_Allocations.m_Large--;

#if defined(RUIN_FREED_MEMORY)
			//memset(ptr, FREED_MEMORY_PATTERN, alloc_size);  // In a protected memory model OS, this would crash because the memory has already been freed back to the OS.
#endif					

			LeaveCriticalSection(&m_MemoryMutex);

#ifdef LZ_USE_MEMTRACE
			MemTrace::HeapFree(m_MemTraceHeapId, ptr);
#endif

			return true;
		}
	}
}
//______________________________________________________________________________________

template <uint kMinSmallAlloc, uint kSmallAllocStepSize, uint kMinMediumAlloc, uint kMediumAllocStepSize, uint kMinLargeAlloc, class OSAPI>
void CMemoryManager<kMinSmallAlloc, kSmallAllocStepSize, kMinMediumAlloc, kMediumAllocStepSize, kMinLargeAlloc, OSAPI>::SanityCheck(void)
{
	for (uint i = 0; i < kNumSmallPages; i++)
	{
		if (m_SmallPages[i])
		{
			ASSERT(m_OSAPI->IsSmallBlock(m_SmallPages[i])!=NULL);  // a page on the small page list doesn't show up as being small.. that's bad.
			m_SmallPages[i]->SanityCheck();
		}
	}
	for (uint i = 0; i < kNumMediumPages; i++)
	{
		if (m_MediumPages[i])
		{
			ASSERT(m_OSAPI->IsMediumBlock(m_MediumPages[i])!=NULL);
			m_MediumPages[i]->SanityCheck();
		}
	}	
}
//______________________________________________________________________________________

template <uint kMinSmallAlloc, uint kSmallAllocStepSize, uint kMinMediumAlloc, uint kMediumAllocStepSize, uint kMinLargeAlloc, class OSAPI>
void CMemoryManager<kMinSmallAlloc, kSmallAllocStepSize, kMinMediumAlloc, kMediumAllocStepSize, kMinLargeAlloc, OSAPI>::GetAllocationInfo(SMemoryData& allocations, SMemoryData& allocated, SMemoryData& top_allocated)
{
	allocations = m_Allocations;
	allocated = m_Allocated;
	top_allocated = m_TopAllocated;
}
//______________________________________________________________________________________


#endif
