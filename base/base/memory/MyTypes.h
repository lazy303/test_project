//-------------------
// MyTypes.h
// Jason Hughes
// Copyright 2010
// Steel Penny Games
//-------------------

#include <stddef.h>
#include <memory.h>
#include <Base/misc/Types.h>
typedef uint32 uint;
typedef uint16 uint16;
typedef uint8 uint8;
