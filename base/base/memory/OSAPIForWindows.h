//-------------------
// COSAPIForWindows.h
// Jason Hughes
// Copyright 2010
// Steel Penny Games
//-------------------

#ifndef OSAPIFORWINDOWS_H
#define OSAPIFORWINDOWS_H

//-------------------

#include <Base/Misc/Assert.h>
#include <set>
#include <map>
#include <windows.h>

//-------------------

class COSAPIForWindows
{
public:
	COSAPIForWindows(void);

	// This is very important to be implemented in a fast way, so free is quick.
	void*	IsSmallBlock (void* ptr) const;  // returns the address of the PAGE this pointer is on
	void*	IsMediumBlock(void* ptr) const;
	bool    IsLargeBlock (void* ptr) const;

	// these functions need to be overridden to change where and how 
	// the memory manager gets small pages.
	void*	AllocSmallPage(void);
	void    FreeSmallPage (void* ptr);

	// these functions need to be overridden to change where and how
	// the memory manager gets medium pages.
	void*	AllocMediumPage(void);
	void    FreeMediumPage (void* ptr);

	// these functions are called whenever the CMemoryManager needs a very large
	// single allocation.  Generally, in Windows this is better to just
	// map to VirtualAlloc, which guarantees no fragmentation but may
	// waste some memory between the end of the allocation and the end
	// of the physical memory page mapped to the address space.  Consoles
	// may direct this to a traditional linked list allocator or manually 
	// handle VirtualAlloc-like behavior by mapping memory blocks.
	void* AllocLarge(uint sz);
	
	// AllocSize is returned so the system can track how much memory is out in each type.
	void  FreeLarge (void* ptr, uint* alloc_size);	

	// definitions that the CMemoryManager will use
	enum
	{
		// both must be a power of 2 for this code to work.  Although the page size
		// is generally 4k, meaning 4k of physical memory can be mapped at a time,
		// the VirtualAlloc function can only dole out memory ADDRESSES in 64k chunks,
		// so if we allocate 4k at a time, we're using up 60k of address space that we
		// will never be able to use.  32 bit programs will run out of address space 
		// before memory in that case.  Very bad.  So we allocate in the optimal chunk
		// sizes... 64k.
		kSmallPageSize  = 64*1024,
		kMediumPageSize = 64*1024,
	};

private:
	// We keep track of only the PAGES for the smaller ones, but we track 
	// individual large allocs, because we really don't want those to leak.
	// Since small and medium allocations are guaranteed to stay on pages,
	// those can't leak anyway.
	std::set<void* > m_SmallPages;  // this is a dumb implementation.  Do something smarter.
	std::set<void* > m_MediumPages;
	std::map<void* , uint>         mLargeAllocs;  // pair of pointers and sizes

	// We fetch this from the OS, and make sure it's what our page sizes are set to.
	uint mNaturalPageSize;
};

//-------------------

inline COSAPIForWindows::COSAPIForWindows(void)
{
	SYSTEM_INFO info;
	GetSystemInfo(&info);
	mNaturalPageSize = info.dwPageSize;

	// If this triggers, you are running in an inoptimal way and should change your 
	// size to match or be a multiple of natural page size of the OS.
	ASSERT(kSmallPageSize % mNaturalPageSize == 0 && kMediumPageSize % mNaturalPageSize == 0);
}

//-------------------

inline void* COSAPIForWindows::IsSmallBlock (void* ptr) const
{
	void* pageStart = (void*)((uint64)ptr & ~(kSmallPageSize-1));  // assume all small pages are aligned to their own size addresses, or better
	if (m_SmallPages.find(pageStart)!=m_SmallPages.end())
		return pageStart;
	return NULL;
}

//-------------------

inline void* COSAPIForWindows::IsMediumBlock(void* ptr) const
{
	void* pageStart = (void*)((uint64)ptr & ~(kMediumPageSize-1));  // assume all medium pages are aligned to their own size addresses, or better
	if (m_MediumPages.find(pageStart)!=m_MediumPages.end())
		return pageStart;
	return NULL;
}

//-------------------

inline bool COSAPIForWindows::IsLargeBlock(void* ptr) const
{
	return mLargeAllocs.find(ptr)!=mLargeAllocs.end();
}

//-------------------

inline void* COSAPIForWindows::AllocSmallPage(void)
{
	void* page_adress = VirtualAlloc(NULL, kSmallPageSize, MEM_COMMIT, PAGE_READWRITE);
	ASSERT(page_adress != nullptr);	
	m_SmallPages.insert(page_adress);
	return page_adress;
}

//-------------------

inline void  COSAPIForWindows::FreeSmallPage(void* ptr)
{
	VirtualFree(ptr, 0, MEM_RELEASE);
	m_SmallPages.erase(ptr);
}

//-------------------

inline void* COSAPIForWindows::AllocMediumPage(void)
{
	void* page_adress = VirtualAlloc(NULL, kMediumPageSize, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
	ASSERT(page_adress != nullptr);
	m_MediumPages.insert(page_adress);
	return page_adress;
}

//-------------------

inline void  COSAPIForWindows::FreeMediumPage (void* ptr)
{
	VirtualFree(ptr, 0, MEM_RELEASE);
	m_MediumPages.erase(ptr);
}

//-------------------

inline void* COSAPIForWindows::AllocLarge(uint sz)
{
	void* addr = VirtualAlloc(NULL, sz, MEM_COMMIT, PAGE_READWRITE);
	ASSERT(addr != nullptr);	
	mLargeAllocs.insert(std::make_pair(addr, sz));
	return addr;
}

//-------------------

inline void  COSAPIForWindows::FreeLarge (void* ptr, uint* alloc_size)
{
	std::map<void* , uint>::iterator i = mLargeAllocs.find(ptr);
	ASSERT(i!=mLargeAllocs.end());  // if this triggers, someone tried to delete a pointer we know nothing about
	if (alloc_size)
		*alloc_size = i->second;
	VirtualFree(ptr, 0, MEM_RELEASE);
	mLargeAllocs.erase(i);
}

//-------------------

#endif
