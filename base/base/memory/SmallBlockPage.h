//-------------------
// CSmallBlockPage.h
// Jason Hughes
// Copyright 2010
// Steel Penny Games
//-------------------

#ifndef FMMSMALLBLOCKPAGE_H
#define FMMSMALLBLOCKPAGE_H

//-------------------

#include <Base/Misc/Assert.h>
#include "MyTypes.h"

//-------------------
// This is a full page worth of allocations of equal sized blocks.
// The basic layout of a small block page goes something like this:
// [start of page]
// [...][...][...][alloc][...][...][alloc][...][...]
// [possible wasted space too small to alloc]
// [allocation bit mask]
// [page tail struct]
// [end of page]
template <uint TPageSize>
struct CSmallBlockPage
{
public:
	// This is called only when a new one is allocated.
	void Initialize(uint block_size);

	// The page literally has no place to store any data except a bitmask 
	// and a pointer at the end of the page, so we calculate where the tail
	// is based on the cache line length and size of the page and tell this class
	// how to work based on that.  The LAST thing we want is to require a cache
	// line fetch in each small block page just to read a few silly configuration
	// variables, anyway.
	// The is_full bool is filled out so that the system knows when the page should
	// be moved to the full pages bucket.
	void* Alloc(uint block_size, bool* is_full);	

	// The hard part of figuring out what page this pointer came from is handled 
	// at a higher level, mainly because all that depends on the policy the user creates.
	// It could be a bitmask of addresses, a simple masking operation on the address
	// itself, or a binary search over a sorted array of addresses, etc.
	// Returns true if the whole page is free, meaning the system can release the whole page.
	// Also fills out whether the page was full before the free occurred, so the system
	// can know which kind of page it came from.
	bool Free(void* ptr, bool* was_full);

	// During the free operation when a page is being removed, we need to figure out the
	// block size so we can pass in the head pointer of the page list, otherwise if a page
	// needs to be freed, it will unlink stuff without the system knowing about it and it
	// will lose track of the page list.
	uint GetBlockSize(void) const;

	// When a new page is added by the system, it asks us to handle the pointer handling.
	void InsertNewHeadPage(CSmallBlockPage** head_ptr);

	// Easy function to untangle the prev/next pointers from the current list.
	void RemoveFromList(CSmallBlockPage** head_ptr);

	// Debugging aid to verify that pointers make sense and bits agree with counts.
	void SanityCheck(void);


	// Debug functions
	uint GetFreeCount() const;
	uint GetAllocationCount() const;
	void DebugOuputAllocationInfo() const;


protected:
	// This will be found at the end of each page.
	struct SPageTail
	{
		// immediately following PRIOR TO this begins the allocation bitmask,
		// which are used as bitwise markers for which chunks in the page are allocated.
		// Note, a FREE block has a SET bit.	
		uint16				m_FreeCount;  // improves alloc and free speed, both.
		uint16				m_BlockSize;  // necessary for Free() to be fast
		CSmallBlockPage*	m_Prev;		// makes restructuring the list faster
		CSmallBlockPage*	m_Next;
	};

	enum
	{
		// we can't precompute how large the bitmask will be because that varies with the
		// size of the block being allocated.  But we can precompute the offset to the
		// start of the tail structure in each page.
		kTailOffset = TPageSize - sizeof(SPageTail),		
	};
	
	// Simple address translation to get to the tail
	SPageTail* GetPageTail (void) const { return (SPageTail*)((char *)this + kTailOffset); }
};


template <uint TPageSize>
inline uint CSmallBlockPage<TPageSize>::GetBlockSize() const
{
	return GetPageTail()->m_BlockSize;
}
//______________________________________________________________________________________

template <uint TPageSize>
void CSmallBlockPage<TPageSize>::Initialize(uint block_size)
{
	// get a pointer to the tail so we can initialize it
	SPageTail* tail = GetPageTail();

	// figure out how many allocatable blocks are actually in this page, 
	// taking into account the size of the tail structure (which includes 
	// a bitmask that depends on the number of blocks in this page!)
	uint const max_allocs_per_page		= (TPageSize - sizeof(SPageTail))/block_size;
	uint const bytes_required_for_tail	= (max_allocs_per_page+31)/32*4 + sizeof(SPageTail);  // in bytes.  Rounded to nearest 32-bit value for performance.
	uint const num_blocks				= (TPageSize - bytes_required_for_tail) / block_size;
	uint const num_bytes_in_bit_mask	= (num_blocks+31)/32*4;

	tail->m_BlockSize = (uint16)block_size;
	tail->m_FreeCount = (uint16)num_blocks;
	tail->m_Next = NULL;
	tail->m_Prev = NULL;

	uint8* bitmask = (uint8*)tail - num_bytes_in_bit_mask;
	SetBitRangeTo1(bitmask, 0, num_blocks-1);  // mark all block alloc masks as FREE
}
//______________________________________________________________________________________

// This function never fails.  It is assumed the system knows there is space available before calling.
template <uint TPageSize>
void* CSmallBlockPage<TPageSize>::Alloc(uint block_size, bool *is_full)
{
	// treat this as a block of memory with a piece of data at the end that we want to access
	SPageTail* tail = GetPageTail();
	ASSERT(tail->m_FreeCount>0);

	// figure out how many allocatable blocks are actually in this page, 
	// taking into account the size of the tail structure (which includes 
	// a bitmask that depends on the number of blocks in this page!)
	uint const max_allocs_per_page		= (TPageSize - sizeof(SPageTail))/block_size;
	uint const bytes_required_for_tail	= (max_allocs_per_page+31)/32*4 + sizeof(SPageTail);  // in bytes.  Rounded to nearest 32-bit value for performance.
	uint const num_blocks				= (TPageSize - bytes_required_for_tail) / block_size;
	uint const num_bytes_in_bit_mask	= (num_blocks+31)/32*4;

	// scan for a free bit, which absolutely should be there
	uint8* bitmask = (uint8*)tail - num_bytes_in_bit_mask;
	uint one_bit_index = FindFirst1BitInRange(bitmask, 0, num_blocks-1);
	ASSERT(one_bit_index!=num_blocks);

	// clear the free bit
	SetBitTo0(bitmask, one_bit_index);

	void* ptr = (uint8*)this + one_bit_index * block_size;

	// move this page to the head of the small list if there's anything left to allocate in it.
	tail->m_FreeCount--;
	*is_full = (tail->m_FreeCount==0);
	return ptr;
}
//______________________________________________________________________________________

template <uint TPageSize>
bool CSmallBlockPage<TPageSize>::Free(void* ptr, bool* was_full)
{
	// treat this as a block of memory with a piece of data at the end that we want to access
	SPageTail* tail = GetPageTail();

	uint const block_size				= tail->m_BlockSize;  // MEMORY ACCESS at the end of the page.  Fetches an L2 cache line.
	uint const max_allocs_per_page		= (TPageSize - sizeof(SPageTail))/block_size;
	uint const bytes_required_for_tail	= (max_allocs_per_page+31)/32*4 + sizeof(SPageTail);  // in bytes.  Rounded to nearest 32-bit value for performance.
	uint const num_blocks				= (TPageSize - bytes_required_for_tail) / block_size;
	uint const num_bytes_in_bit_mask	= (num_blocks+31)/32*4;

	ASSERT(tail->m_FreeCount<num_blocks);  // should be at least one allocated block in this page, or something's broke!
	ASSERT((((uint64)ptr - (uint64)this) % block_size) == 0);  // the address of our pointer should line up evenly on a block start, or something's broke!
	uint8* bitmask = (uint8*)tail - num_bytes_in_bit_mask;  // the bitmask comes right BEFORE the tail structure	
	uint32 const blockNumber = (uint32)((uint64)ptr - (uint64)this) / block_size;

	ASSERT(FindFirst0BitInRange(bitmask, blockNumber, blockNumber)==blockNumber);  // make sure this block is currently free
	SetBitTo1(bitmask, blockNumber);  // mark this block as used
	tail->m_FreeCount++;             // keep account

	*was_full = (tail->m_FreeCount == 1);  // if there's only one free block, this used to be a full page

	// release any page that becomes completely freed
	return (tail->m_FreeCount == num_blocks);
}
//______________________________________________________________________________________

template <uint TPageSize>
void CSmallBlockPage<TPageSize>::InsertNewHeadPage(CSmallBlockPage** head_ptr)
{
	// very simply, insert 'newPage' ahead of this, fixing up both sets of pointers, then assign newPage to *head_ptr.
	SPageTail* tail = GetPageTail();
	CSmallBlockPage* headPage = *head_ptr;
	if (headPage)
	{
		SPageTail* pageTail = headPage->GetPageTail();
		pageTail->m_Prev = this;
	}

	tail->m_Next = headPage;
	*head_ptr = this;
}
//______________________________________________________________________________________

template <uint TPageSize>
void CSmallBlockPage<TPageSize>::SanityCheck(void)
{
	CSmallBlockPage* current_page = this;
	while (current_page)
	{
		SPageTail* tail = current_page->GetPageTail();
		ASSERT(tail->m_Prev==NULL || tail->m_Prev->GetPageTail()->m_Next==current_page);  // prev page must point to us	
		ASSERT(tail->m_Next==NULL || tail->m_Next->GetPageTail()->m_Prev==current_page);  // next page must point to us
	
		// count the bits that are marked as FREE and make sure they match the count in the tail
		uint const block_size				= tail->m_BlockSize;  // MEMORY ACCESS at the end of the page.  Fetches an L2 cache line.
		uint const max_allocs_per_page		= (TPageSize - sizeof(SPageTail))/block_size;
		uint const bytes_required_for_tail	= (max_allocs_per_page+31)/32*4 + sizeof(SPageTail);  // in bytes.  Rounded to nearest 32-bit value for performance.
		uint const num_blocks				= (TPageSize - bytes_required_for_tail) / block_size;
		uint const num_bytes_in_bit_mask	= (num_blocks+31)/32*4;		

		uint8* bitmask = (uint8*)tail - num_bytes_in_bit_mask;  // the bitmask comes right BEFORE the tail structure
		uint bitsSet = 0;
		for (uint i = 0; i<num_blocks; i++)
		{
			if (FindFirst1BitInRange(bitmask, i, i)==i)
			{
				bitsSet++;
			}
		}
		ASSERT(bitsSet == tail->m_FreeCount);  // bits don't match counter
	
		current_page = tail->m_Next;  // check the next page in this list
	}
}
//______________________________________________________________________________________

template <uint TPageSize>
void CSmallBlockPage<TPageSize>::RemoveFromList(CSmallBlockPage** head_ptr)
{
	// very simply, insert this ahead of the head page.
	SPageTail* tail = GetPageTail();
	if (*head_ptr == this)
	{
		*head_ptr = tail->m_Next;  // fixup the head of the list, in case it was us
	}	
	if (tail->m_Prev)
	{
		SPageTail* prevTail = tail->m_Prev->GetPageTail();
		prevTail->m_Next = tail->m_Next;
	}
	if (tail->m_Next)
	{
		SPageTail* nextTail = tail->m_Next->GetPageTail();
		nextTail->m_Prev = tail->m_Prev;	
	}
	tail->m_Next = NULL;
	tail->m_Prev = NULL;
}
//______________________________________________________________________________________

template <uint TPageSize>
uint CSmallBlockPage<TPageSize>::GetAllocationCount() const
{
	SPageTail* tail = GetPageTail();

	// figure out how many allocatable blocks are actually in this page, 
	// taking into account the size of the tail structure (which includes 
	// a bitmask that depends on the number of blocks in this page!)
	uint const max_allocs_per_page		= (TPageSize - sizeof(SPageTail))/tail->m_BlockSize;
	uint const bytes_required_for_tail	= (max_allocs_per_page+31)/32*4 + sizeof(SPageTail);  // in bytes.  Rounded to nearest 32-bit value for performance.
	uint const num_blocks				= (TPageSize - bytes_required_for_tail) / tail->m_BlockSize;

	return num_blocks - tail->m_FreeCount;
}
//______________________________________________________________________________________

template <uint TPageSize>
uint CSmallBlockPage<TPageSize>::GetFreeCount() const
{
	SPageTail* tail = GetPageTail();
	return tail->m_FreeCount;
}
//______________________________________________________________________________________

template <uint TPageSize>
void CSmallBlockPage<TPageSize>::DebugOuputAllocationInfo() const
{
	SPageTail* tail = GetPageTail();

	uint const block_size				= tail->m_BlockSize;  // MEMORY ACCESS at the end of the page.  Fetches an L2 cache line.
	uint const max_allocs_per_page		= (TPageSize - sizeof(SPageTail))/block_size;
	uint const bytes_required_for_tail	= (max_allocs_per_page+31)/32*4 + sizeof(SPageTail);  // in bytes.  Rounded to nearest 32-bit value for performance.
	uint const num_blocks				= (TPageSize - bytes_required_for_tail) / block_size;
	uint const num_bytes_in_bit_mask	= (num_blocks+31)/32*4;
	uint8* bitmask = (uint8*)tail - num_bytes_in_bit_mask;  // the bitmask comes right BEFORE the tail structure	

	char memory[32];
	char buffer[512];

	for (uint block = 0; block < num_blocks;)
	{
		for (uint b = 0; b < 8; ++b)
		{
			if (block >= num_blocks)
				break;			

			uint8 bit = 1 << b;
			if ((*bitmask & bit) == 0)
			{
				char* ptr = (char*)this + block * block_size;
				memcpy(memory, ptr, 32);

				//Visualize 0 as a space
				for (int j = 0; j < 32; ++j)
					if (memory[j] == 0)
						memory[j] = ' ';

				memory[31] = 0;
				sprintf_s(buffer, 512, "{%d} Small block at: %llX, %d bytes long.\nData: <%s>", block, (uint64)ptr, block_size, memory);
				OutputDebugString(buffer);
				for (int j = 0; j < 32; ++j)
				{
					uint tmp = ptr[j] & 0xFF;
					sprintf_s(buffer, 512, " %X", tmp);
					OutputDebugString(buffer);
				}				
				OutputDebugString("\n");
			}

			++block;
		}

		bitmask++;
	}
}
//______________________________________________________________________________________

#endif
