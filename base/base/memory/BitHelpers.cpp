//-------------------
// BitHelpers.h
// Jason Hughes
// Copyright 2010
// Steel Penny Games
//-------------------


#include "BitHelpers.h"

#if 0
//-------------------
// This assumes a bit will be set, so don't call it unless there is.
inline uint FindFirstSetBitInByte(uint v)
{
	ASSERT(v<256);  // don't call this with more than an 8 bit value
	uint firstBitSet[16] = { 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0 };
	
	// the 0th entry will never be used...
	if (v & 0x0f)  // lower nibble
	{
		return firstBitSet[v & 0x0f];
	}
	else  // upper nibble
	{
		return firstBitSet[v >> 4] + 4;
	}
}

// This assumes a bit will be set, so don't call it unless there is.

inline uint FindFirstSetBit(uint v)
{
	if (v & 0xffff)
	{
		if (v & 0xff)  // first byte
		{
			return FindFirstSetBitInByte(v & 0xff);
		}
		else  // second byte
		{
			return FindFirstSetBitInByte((v >> 8) & 0xff) + 8;
		}
	}
	else if (v & 0xff0000)  // third byte
	{
		return FindFirstSetBitInByte((v >> 16) & 0xff) + 16;	
	}
	else  // fourth byte
	{
		return FindFirstSetBitInByte((v >> 24) & 0xff) + 24;	
	}
}
#else
//-------------------
// This returns an index to an 'on' bit in data.  Presumably the lowest on bit.
inline uint FindFirstSetBit(uint data)
{
	uint bit_index = 0xffffffff;
	
#if defined(_M_IX86)
	// use a little assembly to get the index of the first one bit trivially
	_asm
	{
		mov eax, data
		bsf ecx, eax
		jz  nobits
		mov bit_index, ecx
nobits:
	}
#else
	// this does a Log(N) search through 32 bits for the first 'on' bit we can find
	if (data)
	{
		bit_index = 0;
		uint mask = 0x0000ffff;
		for (uint shiftCount=16; shiftCount;)
		{
			if (data & mask)  // if this is true, leave the data where it is
			{
			}
			else  // the on bit is above the bottom half, so shift it down, and increase the bit index by that much
			{
				data     >>= shiftCount;
				bit_index  += shiftCount;
			}

			// slide the mask down so it's half as big
			shiftCount >>= 1;
			mask       >>= shiftCount;
		}
	}
#endif
	return bit_index;
}
#endif
//-------------------
// This scans looking for any set bits, but changes nothing.
uint FindFirst1BitInRange(void const* root, uint bit_start, uint bit_end)
{
	// we require root to be 4-byte address aligned, for memory performance reasons.
	ASSERT(((size_t)root & 3)==0);

	// Now, we operate entirely in 32-bit space. (which means Endian-ness matters)
	unsigned* current_word = (unsigned*)root + bit_start/32;

	uint bist_to_set = bit_end - bit_start + 1;	
	uint bits_left_in_word = (32 - (bit_start & 31));

	// deal with special case where start AND end are in this first word.
	// Otherwise, the mask extends to the end of the first word, possibly over many others, and terminates partway along the last word
	if (bist_to_set < bits_left_in_word)
	{
		uint value = *current_word;
		value >>= bit_start & 31;  // shift the bits we care about down until they are the lowest order bits
		uint mask = ((1 << bist_to_set)-1);  // these are in the low order
		uint overlap = value & mask;
		if (overlap)
		{
			return FindFirstSetBit(overlap) + bit_start;
		}
		else  // didn't find the bit
		{
			return bit_end + 1;
		}
	}
	else  // this mask extends to the end of the word
	{
		uint value = *current_word;
		value >>= bit_start & 31;
		if (value)  // found our bit already
		{
			return FindFirstSetBit(value) + bit_start;
		}
		current_word++;		
		bist_to_set -= bits_left_in_word;		
		bit_start += bits_left_in_word;
	}
	
	// now, as long as there are more than 32 bits to set, simply force-set the whole word w/o reading it from memory.  No need.
	while (bist_to_set >= 32)
	{
		uint value = *current_word++;
		if (value)
		{
			return FindFirstSetBit(value) + bit_start;
		}
		bist_to_set -= 32;
		bit_start  += 32;
	}

	// finally, figure out how many bits we want to actually set in this final word and set only those
	if (bist_to_set>0)
	{
		uint value = *current_word;
		value &= ((1 << bist_to_set)-1);  // keep all bits up to the count remaining
		if (value)
		{
			return FindFirstSetBit(value) + bit_start;
		}
	}
	return bit_end + 1;
}

//-------------------

uint FindFirst0BitInRange(void const* root, uint bit_start, uint bit_end)
{
	// we require root to be 4-byte address aligned, for memory performance reasons.
	ASSERT(((size_t)root & 3)==0);

	// Now, we operate entirely in 32-bit space. (which means Endian-ness matters)
	unsigned      *current_word = (unsigned*)root + bit_start/32;

	uint bist_to_set = bit_end - bit_start + 1;	
	uint bits_left_in_word = (32 - (bit_start & 31));

	// deal with special case where start AND end are in this first word.
	// Otherwise, the mask extends to the end of the first word, possibly over many others, and terminates partway along the last word
	if (bist_to_set < bits_left_in_word)
	{
		uint value = *current_word;
		value >>= bit_start & 31;  // shift the bits we care about down until they are the lowest order bits
		uint mask = ((1 << bist_to_set)-1);  // these are in the low order
		uint overlap = (value & mask) ^ mask;  // flip all 0 bits to 1 bits in the range we care about
		if (overlap)
		{
			return FindFirstSetBit(overlap) + bit_start;
		}
		else  // didn't find the bit
		{
			return bit_end + 1;
		}
	}
	else  // this mask extends to the end of the word
	{
		uint value = *current_word;
		value >>= bit_start & 31;
		uint mask = 0xffffffff >> (bit_start & 31);  // lowest bits mask
		value = (value & mask) ^ mask;
		if (value)  // found our bit already
		{
			return FindFirstSetBit(value) + bit_start;
		}
		current_word++;
		bist_to_set -= bits_left_in_word;		
		bit_start += bits_left_in_word;
	}
	
	// now, as long as there are more than 32 bits to set, simply force-set the whole word w/o reading it from memory.  No need.
	while (bist_to_set >= 32)
	{
		uint value = ~(*current_word++);
		if (value)
		{
			return FindFirstSetBit(value) + bit_start;
		}
		bist_to_set -= 32;
		bit_start  += 32;
	}

	// finally, figure out how many bits we want to actually set in this final word and set only those
	if (bist_to_set>0)
	{
		uint value = *current_word;
		uint mask = ((1 << bist_to_set)-1);
		value = (value & mask) ^ mask;  // flip 0 bits to 1 so we can check for them
		if (value)
		{
			return FindFirstSetBit(value) + bit_start;
		}
	}
	return bit_end + 1;
}

//-------------------
// This is a little helper that can set a range of bits for us.
// Note: We CANNOT assume that root is 32-bit aligned.  We have 
// to handle that ourselves by adjusting bit_start and bit_end to be 
// relative to the proper address.
void SetBitRangeTo1(void* root, uint bit_start, uint bit_end)
{
	// we require root to be 4-byte address aligned, for memory performance reasons.
	ASSERT(((size_t)root & 3)==0);

	// Now, we operate entirely in 32-bit space. (which means Endian-ness matters)
	unsigned      *current_word = (unsigned*)root + bit_start/32;

	uint bist_to_set = bit_end - bit_start + 1;	
	uint bits_left_in_word = (32 - (bit_start & 31));

	// deal with special case where start AND end are in this first word.
	// Otherwise, the mask extends to the end of the first word, possibly over many others, and terminates partway along the last word
	if (bist_to_set < bits_left_in_word)
	{
		uint value = *current_word;
		value |= ((1 << bist_to_set)-1) << (bit_start & 31);  // adjust the mask upward
		*current_word = value;
		return;
	}
	else  // this mask extends to the end of the word
	{
		uint value = *current_word;
		value |= 0xffffffff << (bit_start & 31);  // set all bits at or above the start
		*current_word++ = value;
		bist_to_set -= bits_left_in_word;		
	}
	
	// now, as long as there are more than 32 bits to set, simply force-set the whole word w/o reading it from memory.  No need.
	while (bist_to_set >= 32)
	{
		*current_word++ = 0xffffffff;
		bist_to_set -= 32;
	}

	// finally, figure out how many bits we want to actually set in this final word and set only those
	if (bist_to_set>0)
	{
		uint value = *current_word;
		value |= ((1 << bist_to_set)-1);  // set all bits up to the count remaining
		*current_word = value;
	}
}

//-------------------

void SetBitRangeTo0(void* root, uint bit_start, uint bit_end)
{
	// we require root to be 4-byte address aligned, for memory performance reasons.
	ASSERT(((size_t)root & 3)==0);

	// Now, we operate entirely in 32-bit space. (which means Endian-ness matters)
	unsigned      *current_word = (unsigned*)root + bit_start/32;

	uint bist_to_set = bit_end - bit_start + 1;	
	uint bits_left_in_word = (32 - (bit_start & 31));

	// deal with special case where start AND end are in this first word.
	// Otherwise, the mask extends to the end of the first word, possibly over many others, and terminates partway along the last word
	if (bist_to_set < bits_left_in_word)
	{
		uint value = *current_word;
		value &= ~(((1 << bist_to_set)-1) << (bit_start & 31));  // adjust the mask upward
		*current_word = value;
		return;
	}
	else  // this mask extends to the end of the word
	{
		uint value = *current_word;
		value &= ~(0xffffffff << (bit_start & 31));  // clear all bits at or above the start
		*current_word++ = value;
		bist_to_set -= bits_left_in_word;
	}
	
	// now, as long as there are more than 32 bits to set, simply force-set the whole word w/o reading it from memory.  No need.
	while (bist_to_set >= 32)
	{
		*current_word++ = 0;
		bist_to_set -= 32;
	}

	// finally, figure out how many bits we want to actually set in this final word and set only those
	if (bist_to_set>0)
	{
		uint value = *current_word;
		value &= ~((1 << bist_to_set)-1);  // clear all bits up to the count remaining
		*current_word = value;
	}
}

//-------------------
