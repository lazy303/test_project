#pragma once

namespace NLog
{
	void Print(const char* text, ...);
}