

#include <stdio.h>
#include <Base/Misc/Assert.h>

int AssertMsg(bool statement, const char* desc, const char* file, int line)
{
	if (!statement)
	{
		char str[1024];
		_snprintf_s(str, 1024 - 1, "Assert: %s,\nFile: %s\nLine: %d\n", desc, file, line);
		str[1024 - 1] = 0;

		OutputDebugString(str);

		int v = IDIGNORE;
		if (IsDebuggerPresent())
			v = MessageBox(nullptr, str, "Assertion failed", MB_ABORTRETRYIGNORE | MB_ICONWARNING | MB_TASKMODAL);

		if (v == IDABORT)
		{
			exit(0);
		}
		else if (v == IDRETRY)
		{
			return 1;
		}
	}

	return 0;
}
//___________________________________________________________________________________________
