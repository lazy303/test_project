

#define LZ_ALIGN(n) __declspec(align(n))

typedef unsigned char uint8;
typedef signed char int8;

typedef signed short int16;
typedef unsigned short uint16;

typedef signed int int32;
typedef unsigned int uint32;

typedef signed __int64 int64;
typedef unsigned __int64 uint64;
