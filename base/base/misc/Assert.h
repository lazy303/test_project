
#pragma once

#include <windows.h>

#if defined(DEBUG) || defined(_DEBUG)

	#define _debugbreak_() { if (IsDebuggerPresent()) __debugbreak(); }

#else

	#define _debugbreak_() {}

#endif

extern int AssertMsg(bool statement, const char* desc, const char* file, int line);

#define ASSERT(x) if (AssertMsg(x, #x, __FILE__, __LINE__ )==1){ _debugbreak_(); }
